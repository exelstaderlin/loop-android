package com.pmberjaya.loopmusicandroid.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 6/26/2019.
 */
class SongData() : Parcelable{
    @SerializedName("id")
    @Expose
    var id: Int = -1
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("link_url")
    @Expose
    var linkUrl: String? = null
    @SerializedName("playcount")
    @Expose
    var playcount: String? = null
    @SerializedName("duration")
    @Expose
    var duration: String? = null
    @SerializedName("artist")
    @Expose
    var artist: ArtistData? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        linkUrl = parcel.readString()
        playcount = parcel.readString()
        duration = parcel.readString()
        artist = parcel.readParcelable(ArtistData::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(linkUrl)
        parcel.writeString(playcount)
        parcel.writeString(duration)
        parcel.writeParcelable(artist, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SongData> {
        override fun createFromParcel(parcel: Parcel): SongData {
            return SongData(parcel)
        }

        override fun newArray(size: Int): Array<SongData?> {
            return arrayOfNulls(size)
        }

        operator fun iterator(): Iterator<SongData> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
    }


}