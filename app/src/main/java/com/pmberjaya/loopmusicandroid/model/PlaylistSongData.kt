package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 7/6/2019.
 */
class PlaylistSongData  {

    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("id_playlist")
    @Expose
    val idPlaylist: Int? = null
    @SerializedName("songs")
    @Expose
    val songs: SongData? = null

}