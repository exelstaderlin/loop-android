package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 6/25/2019.
 */
class UserData {
    @SerializedName("id")
    @Expose
    var id: Int = -1
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("registered_date")
    @Expose
    var registeredDate: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("last_login_date")
    @Expose
    var lastLoginDate: String? = null
    @SerializedName("role")
    @Expose
    var role: Int = 0

}