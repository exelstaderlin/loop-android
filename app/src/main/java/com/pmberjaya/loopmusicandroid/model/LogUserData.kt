package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 7/19/2019.
 */
class LogUserData {

    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("id_session")
    @Expose
    val idSession: Int? = null
    @SerializedName("id_user")
    @Expose
    val idUser: Int? = null
    @SerializedName("playcount")
    @Expose
    val playcount: Int? = null
    @SerializedName("created_date")
    @Expose
    val createdDate: String? = null
    @SerializedName("songs")
    @Expose
    val songs: SongData? = null
}