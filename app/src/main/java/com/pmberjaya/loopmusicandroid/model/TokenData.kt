package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Exel staderlin on 7/30/2019.
 */
class TokenData {
    @SerializedName("access_token")
    @Expose
    val accessToken: String? = null
    @SerializedName("refresh_token")
    @Expose
    val refreshToken: String? = null
}