package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Exel staderlin on 2/25/2019.
 */
class UploadFoto {
    @SerializedName("path")
    @Expose
    val photoPath: String? = null
    @SerializedName("src")
    @Expose
    val src: String? = null
}