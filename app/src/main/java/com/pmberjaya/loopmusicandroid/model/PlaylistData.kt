package com.pmberjaya.loopmusicandroid.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 7/4/2019.
 */
class PlaylistData() : Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("id_user")
    @Expose
    var idUser: Int? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null
    @SerializedName("created_date")
    @Expose
    var createdDate: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        idUser = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        image = parcel.readString()
        createdDate = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeValue(idUser)
        parcel.writeString(title)
        parcel.writeString(image)
        parcel.writeString(createdDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaylistData> {
        override fun createFromParcel(parcel: Parcel): PlaylistData {
            return PlaylistData(parcel)
        }

        override fun newArray(size: Int): Array<PlaylistData?> {
            return arrayOfNulls(size)
        }
    }


}