package com.pmberjaya.loopmusicandroid.model

/**
 * Created by Harjot on 09-Dec-16.
 */

class EqualizerModel internal constructor() {
    var isEqualizerEnabled: Boolean = false
    var seekbarpos = IntArray(5)
    var presetPos: Int = 0
    var reverbPreset: Short = 0
    var bassStrength: Short = 0

    init {
        isEqualizerEnabled = true
        reverbPreset = -1
        bassStrength = -1
    }
}
