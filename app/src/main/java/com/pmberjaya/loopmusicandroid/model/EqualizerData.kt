package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 7/19/2019.
 */
class EqualizerData {

    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("id_user")
    @Expose
    val idUser: Int? = null
    @SerializedName("frequency")
    @Expose
    val frequency: String? = null
}