package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RecommendationData(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("id_user")
    @Expose
    val idUser: Int,
    @SerializedName("id_song")
    @Expose
    val idSong: Int,
    @SerializedName("weight")
    @Expose
    val weight: String,
    @SerializedName("songs")
    @Expose
    val songs: SongData? = null
)