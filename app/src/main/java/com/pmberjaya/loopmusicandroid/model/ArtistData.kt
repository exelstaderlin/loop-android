package com.pmberjaya.loopmusicandroid.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 6/26/2019.
 */

class ArtistData() : Parcelable{
    @SerializedName("id")
    @Expose
    var id: Int? = -1
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("image")
    @Expose
    var image: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()
        image = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ArtistData> {
        override fun createFromParcel(parcel: Parcel): ArtistData {
            return ArtistData(parcel)
        }

        override fun newArray(size: Int): Array<ArtistData?> {
            return arrayOfNulls(size)
        }
    }

}