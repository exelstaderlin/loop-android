package com.pmberjaya.loopmusicandroid.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



/**
 * Created by Exel staderlin on 8/12/2019.
 */
class SearchLogData {
    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("id_user")
    @Expose
    val idUser: Int? = null
    @SerializedName("id_playlist")
    @Expose
    val idPlaylist: Any? = null
    @SerializedName("id_song")
    @Expose
    val idSong: Int? = null
    @SerializedName("id_artist")
    @Expose
    val idArtist: Any? = null
    @SerializedName("playlist")
    @Expose
    val playlist: PlaylistData? = null
    @SerializedName("songs")
    @Expose
    val songs: SongData? = null
    @SerializedName("artist")
    @Expose
    val artist: ArtistData? = null

}