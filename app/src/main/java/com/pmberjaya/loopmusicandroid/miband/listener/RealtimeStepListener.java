package com.pmberjaya.loopmusicandroid.miband.listener;

public interface RealtimeStepListener {
  void onNotify(int steps);
}
