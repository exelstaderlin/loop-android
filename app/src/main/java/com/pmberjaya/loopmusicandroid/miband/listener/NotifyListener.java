package com.pmberjaya.loopmusicandroid.miband.listener;

public interface NotifyListener {
  void onNotify(byte[] data);
}
