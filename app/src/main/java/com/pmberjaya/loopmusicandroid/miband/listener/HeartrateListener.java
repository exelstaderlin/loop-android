package com.pmberjaya.loopmusicandroid.miband.listener;

public interface HeartrateListener {
  void onNotify(int heartRate);
}
