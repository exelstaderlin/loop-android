package com.pmberjaya.loopmusicandroid.io

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class BaseRx<T> {
    fun requestNoList(d: Observable<T>, apiCallback: DisposableObserver<T>): CompositeDisposable {

        val mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

        mCompositeDisposable!!.add(
                d
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeWith(apiCallback)
        )
        return mCompositeDisposable
    }
}