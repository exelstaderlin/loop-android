package com.pmberjaya.loopmusicandroid.io

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import com.pmberjaya.loopmusicandroid.model.*
import io.reactivex.Observable
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Created by Exel staderlin on 7/12/2017.
 */
interface ApiInterface {

    @FormUrlEncoded
    @POST("api/login")
    suspend fun goLogin(@FieldMap param : Map<String, String>): BaseDataCallback<UserData>

    @FormUrlEncoded
    @POST("api/forget_password")
    fun resetPassword(@Field("email") email : String): Observable<BaseCallback>

    @FormUrlEncoded
    @POST("api/profile/update/{id}")
    fun updateProfile(@Path("id") id: Int, @FieldMap param : Map<String, String>): Observable<BaseDataCallback<UserData>>

    @FormUrlEncoded
    @POST("api/change_password/{id}")
    fun changePassword(@Path("id") id: Int, @FieldMap param : Map<String, String>): Observable<BaseDataCallback<UserData>>

    @FormUrlEncoded
    @POST("api/register")
    fun goRegister(@FieldMap param : Map<String, String>): Observable<BaseDataCallback<UserData>>

    @GET("api/song")
    fun getAllSong(): Observable<BaseListCallback<SongData>>

    @GET("api/recomendation")
    fun getRecommendationSong(@Query("userId") id_user : Int): Observable<BaseListCallback<RecommendationData>>

    @GET("api/calculate/recomendation")
    fun calculateRecommendationUser(@Query("userId") id_user : Int): Observable<BaseListCallback<RecommendationData>>

    @GET("api/song/artist/{id}")
    fun getSongByArtist(@Path("id") id: String): Observable<BaseListCallback<SongData>>

    @GET("api/playlist/{id}/song")
    fun getSongByPlaylist(@Path("id") id: String): Observable<BaseListCallback<PlaylistSongData>>

    @GET("api/song/{id}")
    fun getSongById(@Path("id") id : Int): Observable<BaseDataCallback<SongData>>

    @GET("api/log/{id_user}")
    fun getRecentlyPlayedSong(@Path("id_user") id: Int): Observable<BaseListCallback<LogUserData>>

    @GET("api/log/count")
    fun getPopularSong(): Observable<BaseListCallback<LogUserData>>

    @FormUrlEncoded
    @POST("api/add/log")
    fun postUserLog(@Field("id_user") idUser : Int, @Field("id_song") idSong : Int): Observable<BaseCallback>

    @GET("api/song/search")
    fun searchSong(@Query("title") title : String): Observable<BaseListCallback<SongData>>

    @GET("api/artist/search")
    fun searchArtist(@Query("title") title : String): Observable<BaseListCallback<ArtistData>>

    @GET("api/playlist/{id_user}/search")
    fun searchPlaylist(@Path("id_user") idUser : String, @Query("title") title : String): Observable<BaseListCallback<PlaylistData>>

    @GET("api/search_log/{id_user}")
    fun getRecentlySearch(@Path("id_user") idUser : Int): Observable<BaseListCallback<SearchLogData>>

    @FormUrlEncoded
    @POST("api/add/song/playlist")
    fun addSongToPlaylist(@Field("id_playlist") idPlaylist : Int, @Field("id_song") idSong : Int): Observable<BaseCallback>

    @FormUrlEncoded
    @POST("api/add/search_log")
    fun postSearchLog(@FieldMap map : HashMap<String, Int>): Observable<BaseCallback>

    @GET("api/artist")
    fun getAllArtist(): Observable<BaseListCallback<ArtistData>>

    @GET("api/playlist")
    fun getPlaylist(@Query("userId") userId : Int): Observable<BaseListCallback<PlaylistData>>

    @GET("api/equalizer")
    fun getEqualizer(@Query("userId") userId : Int): Observable<BaseDataCallback<EqualizerData>>

    @FormUrlEncoded
    @POST("api/add/playlist/{id}")
    fun createPlaylist(@Path("id") id: String, @FieldMap param : Map<String, String>): Observable<BaseCallback>

    @FormUrlEncoded
    @POST("api/edit/playlist/{id}")
    fun editPlaylist(@Path("id") id: String, @Field("image") image : String): Observable<BaseDataCallback<PlaylistData>>

    @FormUrlEncoded
    @POST("api/add/equalizer/{id_user}")
    fun addEqualizer(@Path("id_user") idUser: Int, @Field("frequency") frequency : String): Observable<BaseCallback>

    @DELETE("api/playlist/delete/{id}")
    fun deletePlaylist(@Path("id") id: Int): Observable<BaseCallback>

    @DELETE("api/playlist/{id_playlist}/delete/song/{id_song}")
    fun deleteSongByPlaylist(@Path("id_playlist") id_playlist: Int,@Path("id_song") id_song: Int): Observable<BaseCallback>

}

interface ApiSariputta {    //--------------------------PINJAM SERVER SARIPUTTA------------------------- //

    @FormUrlEncoded
    @POST("api/login_google")
    fun getTokenSariputta(@FieldMap params: Map<String, String>): Observable<BaseDataCallback<TokenData>>

    @POST("api/upload")
    suspend fun uploadAvatar(@Body file: RequestBody): BaseDataCallback<UploadFoto>

}
