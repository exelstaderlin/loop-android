package com.pmberjaya.loopmusicandroid.io

import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io

class BaseDataRx<T> {
    fun request(
        observable: Observable<BaseDataCallback<T>>,
        disposableObserver: DisposableObserver<BaseDataCallback<T>>
    ): CompositeDisposable {
        return CompositeDisposable().apply {
                add(
                        observable
                                .observeOn(mainThread())
                                .subscribeOn(io())
                                .subscribeWith(disposableObserver)
                )
        }

    }

    fun requestList(
        observable: Observable<BaseListCallback<T>>,
        disposableObserver: DisposableObserver<BaseListCallback<T>>
    ) : CompositeDisposable{

        return CompositeDisposable().apply {
                add(
                        observable
                                .observeOn(mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribeWith(disposableObserver)
                )
        }
    }

}