package com.pmberjaya.loopmusicandroid.io

import android.annotation.SuppressLint
import android.app.Activity
import com.pmberjaya.loopmusicandroid.util.SessionManager

/**
 * Created by Exel staderlin on 7/1/2017.
 */
@SuppressLint("StaticFieldLeak")
object Config {

    var url = ""
    private const val PORT = "8080"
    const val DEFAULT_PROFILE_PIC_URL = "https://www.logolynx.com/images/logolynx/b4/b4ef8b89b08d503b37f526bca624c19a.jpeg"
    const val SARIPUTTA_URL = "https://www.sariputta.com/"


    const val DEFAULT_TOKEN_SARIPUTTA = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI3N2FlMjZkYTEzODUxYzNmYTA1YTA2Mjk0ZWM3NjVhYjgzYmUwNzUyY2I2ZjJiOGQzMmEzZWRmNDgxODIyMzUxYThiMTM0NDY0OWM2OWFlIn0.eyJhdWQiOiIyMiIsImp0aSI6IjI3N2FlMjZkYTEzODUxYzNmYTA1YTA2Mjk0ZWM3NjVhYjgzYmUwNzUyY2I2ZjJiOGQzMmEzZWRmNDgxODIyMzUxYThiMTM0NDY0OWM2OWFlIiwiaWF0IjoxNTY0NDc1MjU5LCJuYmYiOjE1NjQ0NzUyNTksImV4cCI6MTg4MDA5NDQ1OSwic3ViIjoiOSIsInNjb3BlcyI6W119.VK368t1zrGSRRRJ6FM8LJcMygsTTLAVlINkLhTdIIEwYOPTQjD0NapIA0zy_35ynQroWK19BT-1GiqvMbNPicTDoqNrQqu38JYxBKmhEgB9imxwvrIlQ8CQqpDfALBvM___oAgJiuJbCCaLJpnw1mnxY1QNVvde6Ba19zoufQxoGk_yLd8IFZ_kvMH6o4bzx0thv2-fTi6gdvAph3ui63B-6Aiqfo6etzEy2vopD7Z1Wmzqv9Us_NueJtlMaKqOzBx_HmoNt3w0IL3hILdFXWjafPsaNQ-BQM77XT8wLWiwKcVTzpq1m90t1kf-zk224fdYfetTBkjv-sDCX-u6nYZ9HUXVOE-WYy2oB4IyaYz8spsP3NnjGDj_sbiql9eeaA2opr7W3dQleib7VZyxUPyys1RtGwU8V3JMmPteqG0PB8Z6cppQsP5KuWZDTBuqGWmLA5WlheiQxdpwqASdVh_TPyF6yM3AvxpSMFGZSi6leJj8STwB4ufq3TuXjDzrn96yD2r4HJ3QONubdC1YzhrV2zwhIdS34Hjeo5xjUjkxat4128AukP21DHgSfyINCwh4tA9aMAL-DXlKRxRuVBoqv8wf6jMnGb5k2ecAXOM3MZ11et7kLCk2g4LYCmXYaZzF9dAOQ8d8-X4Y-IUg-CKBhMVpfG9GIeony9T2QELQ"

    val data_login_google = {
        "name:gery dharmawan\n" +
        "email:gerydharmawan@gmail.com\n" +
        "id:107685049412641928379"
    }

    fun getAppUrl(): String? {
        return url
    }

    fun setAppUrl(activity : Activity){
        url = "http://"+ SessionManager(activity).baseUrl+":$PORT/"
//      url = "https://loop-music-skripsi.herokuapp.com/"
    }


}