package com.pmberjaya.loopmusicandroid.util

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.model.PlaylistData

/**
 * Created by Exel staderlin on 7/18/2019.
 */
class CustomListDialog(var activity: Activity, var title: String, var listData: List<PlaylistData>) {
    fun show(onClick: (id: Int) -> Unit) {
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.list_dialog, null)
        val titles = dialogView.findViewById<TextView>(R.id.alert_dialog_title)
        val recyclerView = dialogView.findViewById<RecyclerView>(R.id.list_item)

        val builder = AlertDialog.Builder(activity).setView(dialogView)
        val dialog = builder.create()

        titles.text = title

        recyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = ListAdapter(listData, onClick =  { id ->
            onClick(id)
            dialog.dismiss()
        })

        dialog.show()
        dialog.setCancelable(true)
    }


    inner class ListAdapter(var datas: List<PlaylistData>, var onClick: (id: Int) -> Unit) :
        RecyclerView.Adapter<ListAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.card_list_title, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return datas.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.titlePlaylist.text = datas[position].title
        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val titlePlaylist = itemView.findViewById<AppCompatTextView>(R.id.title)!!

            init {
                itemView.setOnClickListener {
                    onClick(datas[adapterPosition].id ?: 0)
                }
            }

        }

    }

}