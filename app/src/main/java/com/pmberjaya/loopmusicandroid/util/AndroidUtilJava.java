package com.pmberjaya.loopmusicandroid.util;

import android.os.ParcelUuid;
import android.os.Parcelable;

/**
 * Created by Exel staderlin on 8/2/2019.
 */
public class AndroidUtilJava {

    public static ParcelUuid[] toParcelUuids(Parcelable[] uuids) {
        if (uuids == null) {
            return null;
        }
        ParcelUuid[] uuids2 = new ParcelUuid[uuids.length];
        System.arraycopy(uuids, 0, uuids2, 0, uuids.length);
        return uuids2;
    }


}
