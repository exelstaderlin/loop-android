package com.pmberjaya.loopmusicandroid.util

import com.pmberjaya.loopmusicandroid.model.EqualizerModel

internal object EqualizerSettings {
    var isEqualizerReloaded = true
    var eqPreset = "Custom"
    var seekbarpos = IntArray(5)
    var presetPos: Int = 0
    var equalizerModel: EqualizerModel? = null
    var ratio = 1.0
}
