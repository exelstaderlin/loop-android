package com.pmberjaya.loopmusicandroid.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pmberjaya.loopmusicandroid.model.RecommendationData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.util.SessionManager.SongEnum.*
import com.pmberjaya.loopmusicandroid.util.SessionManager.UserEnum.*
import com.pmberjaya.loopmusicandroid.utilities.Utility
import java.lang.reflect.Type
import java.util.*


/**
 * Created by Exel staderlin on 03/08/2016.
 */

const val SESSION = "session"
const val BASE_URL = "base_url"
const val EQUALIZER = "equalizer"

class SessionManager(context: Context) {

    private val mSharedPreferences: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
    private val mEditor: SharedPreferences.Editor

    init {
        mEditor = mSharedPreferences.edit()
        mEditor.apply()
    }

    var playingSong: SongData
        get() {
            return SongData().apply {
                id = mSharedPreferences.getInt(SONG_ID.name, -1)
                title = mSharedPreferences.getString(SONG_TITLE.name, "")
                linkUrl = mSharedPreferences.getString(SONG_URL.name, "")
                playcount = mSharedPreferences.getString(SONG_PLAYCOUNT.name, "")
                duration = mSharedPreferences.getString(SONG_DURATION.name, "")
                artist = Utility.convertToModel(mSharedPreferences.getString(SONG_ARTIST.name, ""))
            }
        }
        set(value) {
            mEditor.putInt(SONG_ID.name, value.id)
            mEditor.putString(SONG_TITLE.name, value.title)
            mEditor.putString(SONG_URL.name, value.linkUrl)
            mEditor.putString(SONG_PLAYCOUNT.name, value.playcount)
            mEditor.putString(SONG_DURATION.name, value.duration)
            mEditor.putString(SONG_ARTIST.name, Utility.convertToGson(value.artist))
            mEditor.apply()
        }

    var listSongId: ArrayList<Int>
        get() {
            val myList = TextUtils.split(
                mSharedPreferences.getString(LIST_SONG_ID.name, ""),
                "‚‗‚"
            ) as Array<String>
            val arrayToList = Arrays.asList(*myList)
            val newList = ArrayList<Int>()
            for (item in arrayToList)
                newList.add(Integer.parseInt(item))

            return newList
        }
        set(value) {
            val myIntList = value.toArray(arrayOfNulls<Int>(value.size))
            mEditor.putString(LIST_SONG_ID.name, TextUtils.join("‚‗‚", myIntList))
            mEditor.apply()
        }

    var listSongRecommendationData: ArrayList<RecommendationData>
        get() {
            val json = mSharedPreferences.getString(LIST_SONG_RECOMMENDATION.name, "")!!
            var list = ArrayList<RecommendationData>()
            if (json.isNotEmpty()) {
                val type: Type = object : TypeToken<List<RecommendationData>>() {}.type
                list = Gson().fromJson(json, type)
            }
            return list
        }
        set(value) {
            val json = Gson().toJson(value)
            mEditor.putString(LIST_SONG_RECOMMENDATION.name, json)
            mEditor.apply()
        }


    var listSongTemp: ArrayList<SongData>
        get() {
            val json = mSharedPreferences.getString(LIST_SONG_TEMP.name, "")!!
            var list = ArrayList<SongData>()
            if (json.isNotEmpty()) {
                val type: Type = object : TypeToken<List<SongData>>() {}.type
                list = Gson().fromJson(json, type)
            }
            return list
        }
        set(value) {
            val json = Gson().toJson(value)
            mEditor.putString(LIST_SONG_TEMP.name, json)
            mEditor.apply()
        }


    var positionListSongId: Int
        get() {
            return mSharedPreferences.getInt(POSITION_LIST_SONG_ID.name, -1)
        }
        set(value) {
            mEditor.putInt(POSITION_LIST_SONG_ID.name, value)
            mEditor.apply()
        }


    var account: UserData
        get() {
            return UserData().apply {
                id = mSharedPreferences.getInt(USER_ID.name, -1)
                name = mSharedPreferences.getString(USER_NAME.name, "")
                email = mSharedPreferences.getString(USER_EMAIL.name, "")
                registeredDate = mSharedPreferences.getString(USER_CREATE_AT.name, "")
                image = mSharedPreferences.getString(USER_AVATAR.name, "")
                lastLoginDate = mSharedPreferences.getString(USER_UPDATED_AT.name, "")
                role = mSharedPreferences.getInt(USER_ROLE.name, 0)
            }
        }
        set(value) {
            mEditor.putInt(USER_ID.name, value.id)
            mEditor.putString(USER_NAME.name, value.name)
            mEditor.putString(USER_EMAIL.name, value.email)
            mEditor.putString(USER_CREATE_AT.name, value.registeredDate)
            mEditor.putString(USER_AVATAR.name, value.image)
            mEditor.putString(USER_UPDATED_AT.name, value.lastLoginDate)
            mEditor.putInt(USER_ROLE.name, value.role)
            mEditor.apply()
        }

    var hasSession: Boolean
        get() = mSharedPreferences.getBoolean(SESSION, false)
        set(value) {
            mEditor.putBoolean(SESSION, value)
            mEditor.apply()
        }

    var baseUrl: String?
        get() = mSharedPreferences.getString(BASE_URL, "192.168.43.165")
        set(value) {
            mEditor.putString(BASE_URL, value)
            mEditor.apply()
        }

    var equalizerCustom: String?
        get() = mSharedPreferences.getString(EQUALIZER, "0,0,0,0,0,")
        set(value) {
            mEditor.putString(EQUALIZER, value)
            mEditor.apply()
        }

    var switchSleepTracking: Boolean?
        get() = mSharedPreferences.getBoolean(SWITCH.SLEEP_TRACKING.name, false)
        set(value) {
            mEditor.putBoolean(SWITCH.SLEEP_TRACKING.name, value ?: false)
            mEditor.apply()
        }
    var switchShakeAndGesture: Boolean?
        get() = mSharedPreferences.getBoolean(SWITCH.SHAKE_AND_GESTURE.name, false)
        set(value) {
            mEditor.putBoolean(SWITCH.SHAKE_AND_GESTURE.name, value ?: false)
            mEditor.apply()
        }

    var switchEqualizer: Boolean?
        get() = mSharedPreferences.getBoolean(SWITCH.EQUALIZER.name, false)
        set(value) {
            mEditor.putBoolean(SWITCH.EQUALIZER.name, value ?: false)
            mEditor.apply()
        }


    fun clearPlayingSong() {
        mEditor.remove(SONG_ID.name)
        mEditor.remove(SONG_TITLE.name)
        mEditor.remove(SONG_URL.name)
        mEditor.remove(SONG_ARTIST.name)
        mEditor.remove(LIST_SONG_ID.name)
        mEditor.remove(POSITION_LIST_SONG_ID.name)
        mEditor.apply()
    }

    fun clearSession() {
        mEditor.clear()
        mEditor.apply()
    }

    private enum class UserEnum {
        USER_ID,
        USER_NAME,
        USER_EMAIL,
        USER_CREATE_AT,
        USER_AVATAR,
        USER_UPDATED_AT,
        USER_ROLE
    }

    private enum class SongEnum {
        SONG_ID,
        SONG_TITLE,
        SONG_URL,
        SONG_PLAYCOUNT,
        SONG_DURATION,
        SONG_ARTIST,
        LIST_SONG_ID,
        LIST_SONG_RECOMMENDATION,
        LIST_SONG_TEMP,
        POSITION_LIST_SONG_ID
    }

    private enum class SWITCH {
        SLEEP_TRACKING,
        EQUALIZER,
        SHAKE_AND_GESTURE
    }

}