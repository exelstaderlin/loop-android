package com.pmberjaya.loopmusicandroid.utilities


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.google.android.material.textfield.TextInputEditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.text.PrecomputedTextCompat

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import android.text.util.Linkify
import android.view.Window
import android.widget.TextView

import com.google.gson.Gson
import com.pmberjaya.loopmusicandroid.R
import com.squareup.picasso.Picasso
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.regex.Pattern


/**
 * Created by Exel staderlin on 23/07/2016.
 */
object Utility {

    fun fromHtml(html: String?): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html!!.replace("\n", "<br>").replace("<hr>", "<br>"), Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    }

    fun checkInternetConnection(context: Context): Boolean {
        val localNetwork =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return localNetwork != null && localNetwork.isAvailable
    }

//    fun getApi(context: Context): String {
//        val key = SessionKey(context)
//        return key.key ?: ""
//    }

    fun showAlertDialog(activity: Activity, title: String, message: String, yes: () -> Unit, no: () -> Unit) {
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.alerts_dialog, null)
        val titles = dialogView.findViewById<TextView>(R.id.alert_dialog_title)
        val messages = dialogView.findViewById<TextView>(R.id.message)
        val yes = dialogView.findViewById<TextView>(R.id.positive_button)
        val no = dialogView.findViewById<TextView>(R.id.negative_button)

        titles.text = title
        messages.text = message

        val builder = AlertDialog.Builder(activity).setView(dialogView)
        val dialog = builder.create()
        dialog.show()
        dialog.setCancelable(false)

        yes.setOnClickListener {
            dialog.dismiss()
            yes()
        }
        no.setOnClickListener {
            dialog.dismiss()
            no()
        }
    }

    @SuppressLint("InflateParams")
    fun showErrorDialog(context: Activity, message: String) {
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.error_dialog, null)
        val messages = dialogView.findViewById<TextView>(R.id.message)
        val ok = dialogView.findViewById<TextView>(R.id.positive_button)

        val pattern = Pattern.compile(context.getString(R.string.support_link))
        messages.text = message
        Linkify.addLinks(messages, pattern, "https://")

        val builder = AlertDialog.Builder(context).setView(dialogView)
        val dialog = builder.create()
        dialog.show()
        dialog.setCancelable(false)
        ok.setOnClickListener {
            dialog.dismiss()
        }
    }

    @SuppressLint("InflateParams")
    fun showLoadingDialog(context: Activity): AlertDialog {
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.loading_layout, null)

        val builder = AlertDialog.Builder(context).setView(dialogView)
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        return dialog

    }


    fun blankValidator(editText: TextInputEditText): Boolean {
        if (editText.text!!.isBlank()) {
            editText.error = "Field must not be blank"
            return false
        }
        return true
    }

    fun isEmailValid(email: TextInputEditText): Boolean {
        val expression =
            "^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email.text.toString())
        return if (matcher.matches())
            true
        else {
            email.error = "The email is invalid"
            false
        }
    }

    fun cutString(str: String): String {
        var output = str
        if(str.length > 15)
            output= str.substring(0,15) + "..."
        return output
    }

    fun isPasswordValid(password: TextInputEditText): Boolean {
        val pass = password.text.toString()
        return if (!TextUtils.isEmpty(pass) && pass.length >= 6)
            true
        else {
            password.error = "You must have 6 characters in your password"
            false
        }
    }

    fun isPasswordSame(pass: TextInputEditText, confirmPass: TextInputEditText): Boolean {
        return if (pass.text.toString() == confirmPass.text.toString())
            true
        else {
            confirmPass.error = "Check your password again, its not same"
            false
        }
    }

    fun grantAllUriPermissions(context: Context, intent: Intent, uri: Uri?) {
        val resInfoList = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName =
                resolveInfo.activityInfo.packageName  //Comment: Give permission utk membuka file pdf di nougat API 24 versi android 7.0
            context.grantUriPermission(
                packageName,
                uri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }
    }

    fun checkThePermission(context: Context): Boolean {
        val permission = ArrayList<String>()
        val readExt = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)
        val writeExt = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val camera = ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
        val location = ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION)
        val receiveSms = ContextCompat.checkSelfPermission(context, android.Manifest.permission.RECEIVE_SMS)

        when (readExt) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        when (writeExt) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        when (camera) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.CAMERA)
        }
        when (location) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        when (receiveSms) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.RECEIVE_SMS)
        }
        return if (permission.size != 0) {
            requestPermission(context, permission)
            false
        } else true
    }

    private fun requestPermission(context: Context, permission: ArrayList<String>) {
        val arrayPermit = arrayOfNulls<String>(permission.size)
        permission.toArray(arrayPermit)
        ActivityCompat.requestPermissions(context as Activity, permission.toTypedArray(), 1)
    }

    @SuppressLint("Recycle")
    fun getPath(uri: Uri?, activity: Activity): String {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = activity.contentResolver?.query(uri!!, projection, null, null, null)
        val columnIndex = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(columnIndex)
    }

    fun requestMultiPartBody(imagePath: String, imageName: String): MultipartBody? {
        val tipe = imageName.substring(imageName.lastIndexOf(".") + 1)
        val file = File(imagePath)
        val mediaType = MediaType.parse("image/$tipe")
        val requestBody = RequestBody.create(mediaType, file)
        return MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("images", imageName.substring(1, imageName.length), requestBody)
            .build()
    }

    fun intArrToStr(eqIntArr: IntArray): String {
        var eqValueStr = ""
        for (i in eqIntArr.indices) {
            eqValueStr += (eqIntArr[i].toString() + ",")
        }
        return eqValueStr
    }

    fun convertToIntArray(integers: List<Int>): IntArray {
        val ret = IntArray(integers.size)
        for (i in ret.indices)
            ret[i] = integers[i]
        return ret
    }

    fun <T> convertToGson(data: T): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    inline fun <reified T> convertToModel(data: String?): T {
        val gson = Gson()
        return gson.fromJson(data, T::class.java)
    }

    fun supportsBluetoothLE(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
    }

    fun getBitmap(url: String?): Bitmap? {
        var bmp: Bitmap? = null
        Picasso.get().load(url).into(object : com.squareup.picasso.Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bmp = bitmap
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
        })
        return bmp
    }

    fun getImageUri(context: Context, bitmap: Bitmap) : Uri{
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path)
   }

    fun getProgressPercentage(currentDuration: Int, totalDuration: Int): Int {

        val currentSeconds = (currentDuration / 1000).toLong()
        val totalSeconds = (totalDuration / 1000).toLong()

        // calculating percentage
        val percentage = currentSeconds.toDouble() / totalSeconds * 100

        // return percentage
        return percentage.toInt()
    }

    fun milliSecondsToTimer(milliseconds: Long): String {
        var finalTimerString = ""
        val secondsString: String
        // Convert total duration into time
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (100 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        // Add hours if there
        if (hours > 0) {
            finalTimerString = "$hours:"
        }
        // Prepending 0 to seconds if it is one digit
        secondsString = when {
            seconds < 10 -> "0$seconds"
            else -> "" + seconds
        }
        finalTimerString = "$finalTimerString$minutes:$secondsString"
        // return timer string
        return finalTimerString
    }


    fun progressToTimer(progress: Int, totalDuration: Int): Int {
        val duration = totalDuration / 1000
        val currentDuration = (progress.toDouble() / 100 * duration).toInt()
        return currentDuration * 1000
    }

    fun setPrecomputedText(view : AppCompatTextView, text : String) {
        view.setTextFuture(
            PrecomputedTextCompat.getTextFuture(
                text,
                view.textMetricsParamsCompat,
                null
            )
        )
    }

}