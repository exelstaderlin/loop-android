package com.pmberjaya.loopmusicandroid.app.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.home.HomeFragment
import com.pmberjaya.loopmusicandroid.app.music.MusicPlayerActivity
import com.pmberjaya.loopmusicandroid.app.playlist.PlaylistAddDialog
import com.pmberjaya.loopmusicandroid.app.playlist.PlaylistFragment
import com.pmberjaya.loopmusicandroid.app.search.SearchFragment
import com.pmberjaya.loopmusicandroid.app.setting.SettingFragment
import com.pmberjaya.loopmusicandroid.service.ServicesManager
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File


@ExperimentalCoroutinesApi
class MainActivity : BaseActivity() {

    private lateinit var mViewModel: MainViewModel
    private lateinit var mSessionManager: SessionManager
    var playlistAddDialog: PlaylistAddDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setToolbar()
        initObject()
        initListener()
        setupViewPager(MyPagerAdapter(supportFragmentManager))
        //BottomNavigationViewHelper.disableShiftMode(bottom_navigation_view)
        // di update 28.0.0 disable shift_mode sudah bisa langsung dari XML dengan method app:labelVisibilityMode="labeled"
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = "Home"
    }

    private fun initObject() {
        mViewModel = ViewModelProvider(
            this
        ).get(MainViewModel::class.java)
        mSessionManager = SessionManager(this)
        playlistAddDialog = PlaylistAddDialog(this)
    }

    private fun initListener() {
        bottom_navigation_view.setOnNavigationItemSelectedListener { item: MenuItem ->
            item.isChecked = true
            when (item.itemId) {
                R.id.home -> setMenu(0)
                R.id.search -> setMenu(1)
                R.id.playlist -> setMenu(2)
                R.id.setting -> setMenu(3)
            }
            false
        }

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                setMenu(position)
            }
        })

        bottom_music_player.setOnClickListener {
            val intent = Intent(this, MusicPlayerActivity::class.java).apply {
                putExtra("data", mSessionManager.playingSong)
                putExtra("from", "main")
                putIntegerArrayListExtra("list_song_id", mSessionManager.listSongId)
                putParcelableArrayListExtra("list_song_temp", mSessionManager.listSongTemp)
                putExtra("position", mSessionManager.positionListSongId)
            }
            startActivity(intent)
//            startActivity(intent)
        }

    }

    private fun initMusicPlayer() {
        when {
            ServicesManager.getRunningService() -> setMusicPlayer(
                View.VISIBLE,
                mSessionManager.playingSong.title,
                mSessionManager.playingSong.artist?.name
            )
            else -> setMusicPlayer(View.GONE, "", "")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setMusicPlayer(visibility: Int, title: String?, artist: String?) {
        bottom_music_player.visibility = visibility
        text_music_player.isSelected = true
        val playOrPause = if (ServicesManager.isPlaying)
            "is playing now"
        else
            "is pausing now"
        text_music_player.text = "$title ● $artist ● $playOrPause"

    }

    private fun setupViewPager(adapter: PagerAdapter) {
        view_pager.offscreenPageLimit = 4
        view_pager.isPagingEnabled = true
        view_pager.adapter = adapter
    }

    private fun setMenu(currentItem: Int) {
        view_pager.currentItem = currentItem
        bottom_navigation_view.menu.getItem(currentItem).isChecked = true
        when (currentItem) {
            0 -> supportActionBar!!.title = "Homepage"
            1 -> supportActionBar!!.title = "Search"
            2 -> supportActionBar!!.title = "Playlist"
            3 -> supportActionBar!!.title = "Setting"
        }
    }

    fun selectImage() {
        try {
            val pm = packageManager
            val hasPerm = pm?.checkPermission(Manifest.permission.CAMERA, packageName)

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                val options = arrayOf<CharSequence>("Choose From Gallery", "Cancel")
                val builder = AlertDialog.Builder(this)
                builder.setItems(options) { dialog, item ->
                    when {
                        options[item] == "Choose From Gallery" -> {
                            dialog.dismiss()
                            val pickPhoto = Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                            )
                            startActivityForResult(pickPhoto, 1)
                        }
                        options[item] == "Cancel" -> dialog.dismiss()
                    }
                }
                builder.show()
            } else {
                Utility.checkThePermission(this)
            }
        } catch (e: Exception) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        lateinit var imagePath: String
        lateinit var imageName: String
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val imageUri = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                    imagePath = Utility.getPath(imageUri, this)
                    val file = File(imagePath)
                    Log.d("file", "size :" + file.length())
                    if (file.length() > 2000000) {
                        Toast.makeText(applicationContext, "Image too large", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        imageName = imagePath.substring(imagePath.lastIndexOf("/"))
                        playlistAddDialog?.setImage(bitmap!!)
                        playlistAddDialog?.setImageToUpload(imagePath, imageName)
//                        uploadFoto(imagePath, imageName)
                    }

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        initMusicPlayer()
    }
}

class MyPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var tabItems = 4

    override fun getItem(pos: Int): Fragment {
        return when (pos) {
            0 -> HomeFragment()
            1 -> SearchFragment()
            2 -> PlaylistFragment()
            3 -> SettingFragment()
            else -> HomeFragment()
        }
    }

    override fun getCount(): Int {
        return tabItems
    }
}
