package com.pmberjaya.loopmusicandroid.app.search

import android.content.Intent
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.playlist.PlaylistDetailActivity
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.squareup.picasso.Picasso

/**
 * Created by Exel staderlin on 8/12/2019.
 */

class PlaylistAdapterSearch(var datas: List<PlaylistData>, var onClick: (id: Int) -> Unit) :
    RecyclerView.Adapter<PlaylistAdapterSearch.HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_playlist, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        Log.d("position", position.toString())
        holder.titlePlaylist.text = datas[position].title
        holder.titleCreator.text = ""
        Picasso.get()
            .load(datas[position].image)
            .placeholder(R.drawable.music_image)
            .error(R.drawable.music_image)
            .into(holder.cardViewImage)
    }

    inner class HomeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardViewImage = itemView.findViewById<AppCompatImageView>(R.id.image)
        val titlePlaylist = itemView.findViewById<AppCompatTextView>(R.id.title_playlist)
        val titleCreator = itemView.findViewById<AppCompatTextView>(R.id.title_creator)

        init {
            itemView.setOnClickListener {
                onClick(datas[adapterPosition].id!!)
                val intent = Intent(itemView.context, PlaylistDetailActivity::class.java).apply {
                    putExtra("label", "Your songs")
                    putExtra("data", datas[adapterPosition])
                }
                itemView.context.startActivity(intent)
            }
        }
    }


}