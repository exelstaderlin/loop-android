package com.pmberjaya.loopmusicandroid.app.playlist

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainViewModel
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.pmberjaya.loopmusicandroid.model.PlaylistSongData
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.activity_playlist_detail.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.android.synthetic.main.song_list_layout.*
import java.io.File

/**
 * Created by Exel staderlin on 6/21/2019.
 */
class PlaylistDetailActivity : BaseActivity() {

    private var label = ""
    private lateinit var playlistData: PlaylistData
    private lateinit var mViewModel: PlaylistViewModel
    private lateinit var mMainViewModel: MainViewModel
    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_detail)
        setToolbar()
        initObject()
        initListener()
        initObserver()
        initView()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Playlist"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        label = intent.getStringExtra("label") ?: ""
        playlistData = intent.getParcelableExtra("data")
        mViewModel = ViewModelProvider(this).get(PlaylistViewModel::class.java)
        mMainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private fun initListener() {
        add_song_button.setOnClickListener {
            val intent = Intent(this, PlaylistAddSongActivity::class.java)
            intent.putExtra("id_playlist", playlistData.id)
            startActivity(intent)
        }

        circleImageView.setOnClickListener {
            selectImage()
        }
    }

    private fun initObserver() {
        mViewModel.playlistSongResponse.observe(this, Observer {
            when {
                it?.status!! && it.data.size != 0 -> setAdapterPlaylist(it.data)
                else -> title_label.text = it.message
            }
        })

        mViewModel.deleteResponse.observe(this, Observer {
            when {
                it?.status!! -> getPlaylistSong()
                else -> title_label.text = it.message
            }
        })

        mViewModel.editResponse.observe(this, Observer {
            loading_layout.visibility = GONE
            when {
                it?.status!! -> {
                    Picasso.get()
                        .load(it.data?.image)
                        .placeholder(R.drawable.music_image)
                        .error(R.drawable.music_image)
                        .into(circleImageView)
                }
                else -> title_label.text = it.message
            }
        })


        mMainViewModel.uploadAvatarResponse.observe(this, Observer {
            when {
                it!!.status -> {
                    editPlaylist(it.data?.photoPath!!)
                }
                else -> Toast.makeText(this, "Upload failed", Toast.LENGTH_SHORT).show()
            }
        })


        defaultErrorMessageHandling(mViewModel.errorResponse)
    }

    private fun initView() {
        title_label.text = label
        title_playlist.text = playlistData.title
        Picasso.get()
            .load(playlistData.image)
            .placeholder(R.drawable.music_image)
            .error(R.drawable.music_image)
            .into(circleImageView)
    }

    private fun getPlaylistSong() {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.getPlaylistSong("", playlistData.id!!)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    fun deleteSongByPlaylist(songId: Int) {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.deleteSongByPlaylist("", playlistData.id!!, songId)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun editPlaylist(imageUrl: String) {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.editPlaylist("", playlistData.id!!, imageUrl)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun uploadImage(imagePath: String, imageName: String) {
        loading_layout.visibility = View.VISIBLE
        when {
            Utility.checkInternetConnection(this) -> mMainViewModel.uploadImage(
                Utility.requestMultiPartBody(
                    imagePath,
                    imageName
                )!!
            )
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun setAdapterPlaylist(data: List<PlaylistSongData>) {
        song_list_recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        song_list_recycler_view.setHasFixedSize(true)
        song_list_recycler_view.adapter = PlaylistSongAdapter(this, data)
    }

    private fun selectImage() {
        try {
            val pm = packageManager
            val hasPerm = pm?.checkPermission(Manifest.permission.CAMERA, packageName)

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                val options = arrayOf<CharSequence>("Choose From Gallery", "Cancel")
                val builder = AlertDialog.Builder(this)
                builder.setItems(options) { dialog, item ->
                    when {
                        options[item] == "Choose From Gallery" -> {
                            dialog.dismiss()
                            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                            startActivityForResult(pickPhoto, 1)
                        }
                        options[item] == "Cancel" -> dialog.dismiss()
                    }
                }
                builder.show()
            } else {
                Utility.checkThePermission(this)
            }
        } catch (e: Exception) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        lateinit var imagePath: String
        lateinit var imageName: String
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val imageUri = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                    imagePath = Utility.getPath(imageUri, this)
                    val file = File(imagePath)
                    Log.d("file", "size :" + file.length())
                    if (file.length() > 2000000) {
                        Toast.makeText(applicationContext, "Image too large", Toast.LENGTH_SHORT).show()
                    } else {
                        imageName = imagePath.substring(imagePath.lastIndexOf("/"))
                        uploadImage(imagePath, imageName)
                    }

                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
        getPlaylistSong()
    }
}