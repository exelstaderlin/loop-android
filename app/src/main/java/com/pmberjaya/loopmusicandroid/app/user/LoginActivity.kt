package com.pmberjaya.loopmusicandroid.app.user

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainActivity
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.pmberjaya.loopmusicandroid.util.SessionManager
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.loading_layout.*
import kotlinx.coroutines.ExperimentalCoroutinesApi


/**
 * Created by Exel staderlin on 6/5/2018.
 */


class LoginActivity : BaseActivity() {
    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initObject()
        initListener()
        initObserver()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(
            this,
            LoginViewModel.FACTORY(UserRepository())
        ).get(LoginViewModel::class.java)
    }

    @ExperimentalCoroutinesApi
    private fun initListener() {
        btn_login.setOnClickListener {
            when {
                Utility.checkInternetConnection(this) -> {
                    if (Utility.blankValidator(email_edit_text) &&
                        Utility.blankValidator(password_edit_text) &&
                        Utility.isEmailValid(email_edit_text) &&
                        Utility.isPasswordValid(password_edit_text)
                    ) {
                        val param = paramLogin()
                        mViewModel.goLogin(param)
                        loading_layout.visibility = View.VISIBLE
                    }
                }
                else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
            }
        }

        forgot_password.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }
    }


    private fun initObserver() {
        mViewModel.loginResponse.observe(this, Observer<BaseDataCallback<UserData>> {
            loading_layout.visibility = GONE
            when {
                it?.status!! && it.data != null -> {
                    mSessionManager.account = it.data!!
                    mSessionManager.hasSession = true
                    intentToMain()
                }
                else -> Utility.showErrorDialog(this, it.message!!)
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            loading_layout.visibility = GONE
        })

    }

    private fun intentToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        Toast.makeText(this, getString(R.string.login_success), Toast.LENGTH_LONG).show()
        finishAffinity()
    }

    private fun paramLogin(): HashMap<String, String> {
        val param = HashMap<String, String>()
        param["email"] = email_edit_text.text.toString()
        param["password"] = password_edit_text.text.toString()
        return param
    }


}