package com.pmberjaya.loopmusicandroid.app.user

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.UserData
import io.reactivex.observers.DisposableObserver

class RegisterViewModel(application: Application) : AndroidViewModel(application) {

    var registerResponse by MutableLiveDataInitialization<BaseDataCallback<UserData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun goRegister(token: String, _param : HashMap<String,String>) {
        val observable = RestClient.getApiInterface(token).goRegister(_param)
        BaseDataRx<UserData>().request(observable, object : DisposableObserver<BaseDataCallback<UserData>>(){
            override fun onComplete() {
            }

            override fun onNext(t: BaseDataCallback<UserData>) {
                registerResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }


}