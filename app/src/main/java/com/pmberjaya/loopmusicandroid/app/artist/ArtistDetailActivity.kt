package com.pmberjaya.loopmusicandroid.app.artist

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.home.HomeViewModel
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.search.SongListAdapter
import com.pmberjaya.loopmusicandroid.model.ArtistData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_artist_detail.*
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.song_list_layout.*

/**
 * Created by Exel staderlin on 6/21/2019.
 */
class ArtistDetailActivity : BaseActivity() {

    private var label = ""
    private lateinit var artistData : ArtistData
    private lateinit var mViewModel : HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_detail)
        setToolbar()
        initObject()
        initView()
        initObserver()
        getSongByArtist()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Artist"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        label = intent.getStringExtra("label") ?: ""
        artistData = intent.getParcelableExtra("data")
        mViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    private fun initView() {
        title_label.text = label
        title_artist.text = artistData.name
        Picasso.get()
            .load(artistData.image)
            .placeholder(R.drawable.music_image)
            .error(R.drawable.music_image)
            .into(circleImageView)
    }

    private fun initObserver() {
        mViewModel.songResponse.observe(this, Observer {
            when {
                it?.status!! && it.data.size != 0 -> {
                    setAdapterSong(it.data)
                }
                else -> Utility.showErrorDialog(this, it.message!!)
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse)
    }

    private fun getSongByArtist() {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.getSongByArtist("", artistData.id.toString())
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }


    private fun setAdapterSong(data : List<SongData>) {
        song_list_recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        song_list_recycler_view.setHasFixedSize(true)
        song_list_recycler_view.adapter = SongListAdapter(data){/*Nothing*/}
    }

}