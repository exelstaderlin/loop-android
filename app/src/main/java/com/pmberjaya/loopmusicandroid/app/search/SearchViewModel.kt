package com.pmberjaya.loopmusicandroid.app.search

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.BaseRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.ArtistData
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.pmberjaya.loopmusicandroid.model.SearchLogData
import com.pmberjaya.loopmusicandroid.model.SongData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 7/16/2019.
 */
class SearchViewModel : ViewModel() {

    var recentlySearch by MutableLiveDataInitialization<BaseListCallback<SearchLogData>>()
    var searchResponse by MutableLiveDataInitialization<BaseListCallback<SongData>>()
    var artistResponse by MutableLiveDataInitialization<BaseListCallback<ArtistData>>()
    var playlistResponse by MutableLiveDataInitialization<BaseListCallback<PlaylistData>>()
    var addedResponse by MutableLiveDataInitialization<BaseCallback>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun getRecentlySearch(token: String, idUser: Int) {
        val observable = RestClient.getApiInterface(token).getRecentlySearch(idUser)
        BaseDataRx<SearchLogData>().requestList(
            observable,
            object : DisposableObserver<BaseListCallback<SearchLogData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: BaseListCallback<SearchLogData>) {
                    recentlySearch.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })

    }

    fun searchSong(token: String, title: String) {
        val observable = RestClient.getApiInterface(token).searchSong(title)
        BaseDataRx<SongData>().requestList(observable, object : DisposableObserver<BaseListCallback<SongData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<SongData>) {
                searchResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun searchArtist(token: String, title: String) {
        val observable = RestClient.getApiInterface(token).searchArtist(title)
        BaseDataRx<ArtistData>().requestList(observable, object : DisposableObserver<BaseListCallback<ArtistData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<ArtistData>) {
                artistResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun searchPlaylist(token: String, idUser: String, title: String) {
        val observable = RestClient.getApiInterface(token).searchPlaylist(idUser, title)
        BaseDataRx<PlaylistData>().requestList(
            observable,
            object : DisposableObserver<BaseListCallback<PlaylistData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: BaseListCallback<PlaylistData>) {
                    playlistResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun postSearchLog(token: String, param: HashMap<String, Int>) {
        val observable = RestClient.getApiInterface(token).postSearchLog(param)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                addedResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }

    fun addSongToPlaylist(token: String, playlistId: Int, songId: Int) {
        val observable = RestClient.getApiInterface(token).addSongToPlaylist(playlistId, songId)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                addedResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }

}