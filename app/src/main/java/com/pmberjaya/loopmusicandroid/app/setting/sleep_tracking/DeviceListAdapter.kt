/*  Copyright (C) 2015-2019 Andreas Shimokawa, Carsten Pfeiffer

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
package com.pmberjaya.loopmusicandroid.app.setting.sleep_tracking

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.pmberjaya.loopmusicandroid.R


class DeviceCandidateAdapter(var activity: SleepTrackingActivity,var datas: List<DeviceData>) : RecyclerView.Adapter<DeviceCandidateAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_device_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val device = datas[position]
        val name = device.name
        holder.deviceNameLabel.text = name
        holder.deviceAddressLabel.text = device.macAddress
        holder.status.text = device.status
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val deviceNameLabel = itemView.findViewById(R.id.item_name) as TextView
        val deviceAddressLabel = itemView.findViewById(R.id.item_details) as TextView
        val status = itemView.findViewById(R.id.status) as TextView
        init {
            itemView.setOnClickListener {
                activity.startPairing(datas[adapterPosition].device!!)
            }
        }

    }



}