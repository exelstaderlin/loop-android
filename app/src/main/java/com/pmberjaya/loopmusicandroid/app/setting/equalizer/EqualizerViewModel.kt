package com.pmberjaya.loopmusicandroid.app.setting.equalizer

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.BaseRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.EqualizerData
import com.pmberjaya.loopmusicandroid.model.UserData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 7/19/2019.
 */
class EqualizerViewModel : ViewModel() {

    var eqResponse by MutableLiveDataInitialization<BaseDataCallback<EqualizerData>>()
    var addResponse by MutableLiveDataInitialization<BaseCallback>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun getEqualizer(token: String, id: Int) {
        val observable = RestClient.getApiInterface(token).getEqualizer(id)
        BaseDataRx<EqualizerData>().request(observable, object : DisposableObserver<BaseDataCallback<EqualizerData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseDataCallback<EqualizerData>) {
                eqResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun addEqualizer(token: String, id: Int, frequency : String) {
        val observable = RestClient.getApiInterface(token).addEqualizer(id, frequency)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                addResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

}