package com.pmberjaya.loopmusicandroid.app.music

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.pmberjaya.loopmusicandroid.R
import kotlinx.android.synthetic.main.activity_equalizer.toolbar
import kotlinx.android.synthetic.main.fragment_recommendation_music.*


class MusicRecommendationFragment : Fragment() {

//    onClick: () -> Unit

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recommendation_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbar()
        initObject()
        initListener()
    }

    private fun setToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(toolbar as Toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowHomeEnabled(true)
        (activity as AppCompatActivity).supportActionBar!!.title = "Recommendation Music"
    }

    private fun initObject() {

    }

    private fun initListener() {
        btn_cancel.setOnClickListener {
            Toast.makeText(activity, "asdasda", Toast.LENGTH_SHORT).show()
        }
    }


}
