package com.pmberjaya.loopmusicandroid.app.playlist

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.model.SongData

/**
 * Created by Exel staderlin on 7/18/2019.
 */
class AddSongListAdapter(var datas: List<SongData>) : RecyclerView.Adapter<AddSongListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_list_add_song, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleSong.text = datas[position].title
        holder.titleArtist.text  = datas[position].artist?.name
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val titleSong = itemView.findViewById<AppCompatTextView>(R.id.title_song)!!
        val titleArtist = itemView.findViewById<AppCompatTextView>(R.id.title_artist)!!
        private val addSongBtn = itemView.findViewById<AppCompatImageView>(R.id.add_song_button)

        init {
            addSongBtn.setOnClickListener {
                (itemView.context as PlaylistAddSongActivity).addSongToPlaylist(datas[adapterPosition].id)
            }
        }

    }

}