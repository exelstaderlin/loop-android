package com.pmberjaya.loopmusicandroid.app.artist

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.ArtistData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 6/26/2019.
 */
class ArtistViewModel : ViewModel() {

    var artistResponse by MutableLiveDataInitialization<BaseListCallback<ArtistData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun getAllArtist(token: String) {
        val observable = RestClient.getApiInterface(token).getAllArtist()
        BaseDataRx<ArtistData>().requestList(observable, object : DisposableObserver<BaseListCallback<ArtistData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<ArtistData>) {
                artistResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }

}