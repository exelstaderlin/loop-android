package com.pmberjaya.loopmusicandroid.app.setting.account

import android.Manifest
import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainViewModel
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_account.*
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.loading_layout.*
import java.io.File

/**
 * Created by Exel staderlin on 7/6/2019.
 */
class EditAccountActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: AccountViewModel
    private lateinit var mMainViewModel: MainViewModel
    private var imagePath: String = ""
    private var imageName: String = ""
    private var imageUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_account)
        setToolbar()
        initObject()
        initListener()
        initView()
        initObserver()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Edit Account"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(AccountViewModel::class.java)
        mMainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        imageUrl = mSessionManager.account.image.toString()
    }

    private fun initListener() {
        change_pass_button.setOnClickListener {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        update_button.setOnClickListener {
            when {
                Utility.checkInternetConnection(this) -> {
                    if (imagePath != "" || imageName != "") {
                        uploadImage(imagePath, imageName)
                    } else {
                        updateProfile()
                    }
                }
                else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
            }
        }

        circleImageView.setOnClickListener {
            selectImage()
        }
    }

    private fun initView() {
        Picasso.get().load(mSessionManager.account.image).error(R.drawable.music_image).into(circleImageView)
        email_edit_text.setText(mSessionManager.account.email)
        name_edit_text.setText(mSessionManager.account.name)
    }

    private fun initObserver() {

        mViewModel.updateResponse.observe(this, Observer<BaseDataCallback<UserData>> {
            loading_layout.visibility = GONE
            if (it?.status!! && it.data != null) {
                mSessionManager.account = it.data!!
                Snackbar.make(
                    findViewById(android.R.id.content),
                    it.message.toString(),
                    Snackbar.LENGTH_LONG
                )
                    .show()
            } else {
                Utility.showErrorDialog(this, it.message.toString())
            }
        })

        mMainViewModel.uploadAvatarResponse.observe(this, Observer {
            when {
                it!!.status -> {
                    imageUrl = it.data?.photoPath.toString()
                    updateProfile()
                }
                else -> Toast.makeText(this, "Upload failed", Toast.LENGTH_SHORT).show()
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            loading_layout.visibility = GONE
        })

    }

    private fun updateProfile() {
        if (Utility.blankValidator(email_edit_text) &&
            Utility.isEmailValid(email_edit_text)
        ) {
            mViewModel.updateProfile("", mSessionManager.account.id, paramUpdate())
            loading_layout.visibility = VISIBLE
        }

    }

    private fun uploadImage(imagePath: String, imageName: String) {
        loading_layout.visibility = VISIBLE
        when {
            Utility.checkInternetConnection(this) -> mMainViewModel.uploadImage(
                Utility.requestMultiPartBody(
                    imagePath,
                    imageName
                )!!
            )
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun selectImage() {
        try {
            val pm = packageManager
            val hasPerm = pm?.checkPermission(Manifest.permission.CAMERA, packageName)

            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                val options = arrayOf<CharSequence>("Choose From Gallery", "Cancel")
                val builder = AlertDialog.Builder(this)
                builder.setItems(options) { dialog, item ->
                    when {
                        options[item] == "Choose From Gallery" -> {
                            dialog.dismiss()
                            val pickPhoto = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                            startActivityForResult(pickPhoto, 1)
                        }
                        options[item] == "Cancel" -> dialog.dismiss()
                    }
                }
                builder.show()
            } else {
                Utility.checkThePermission(this)
            }
        } catch (e: Exception) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show()
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val imageUri = data.data
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                    imagePath = Utility.getPath(imageUri, this)
                    val file = File(imagePath)
                    Log.d("file", "size :" + file.length())
                    if (file.length() > 2000000) {
                        Toast.makeText(applicationContext, "Image too large", Toast.LENGTH_SHORT).show()
                    } else {
                        imageName = imagePath.substring(imagePath.lastIndexOf("/"))
                        Picasso.get().load(Utility.getImageUri(this, bitmap)).error(R.drawable.music_image)
                            .into(circleImageView)
//                        uploadImage(imagePath, imageName)
                    }

                }
            }
        }

    }

    private fun paramUpdate(): HashMap<String, String> {
        val param = HashMap<String, String>()
        param["name"] = name_edit_text.text.toString()
        param["email"] = email_edit_text.text.toString()
        param["image"] = imageUrl
        return param
    }

}