package com.pmberjaya.loopmusicandroid.app.artist

import android.content.Intent
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.model.ArtistData
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import io.supercharge.shimmerlayout.ShimmerLayout

class ArtistAdapter(var datas: List<ArtistData>, var onClick: (id: Int) -> Unit) :
    Adapter<ArtistAdapter.HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_artist, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        when (datas[position].id) {
            -1 -> holder.skeletonView.startShimmerAnimation()
            else -> holder.skeletonView.stopShimmerAnimation()
        }
        holder.titleArtist.text = Utility.cutString(datas[position].name ?: "")
        Picasso.get()
            .load(datas[position].image)
            .placeholder(R.drawable.music_image)
            .error(R.drawable.music_image)
            .into(holder.cardViewImage)

    }

    inner class HomeViewHolder(itemView: View) : ViewHolder(itemView) {
        val cardViewImage = itemView.findViewById<CircleImageView>(R.id.image)!!
        val titleArtist = itemView.findViewById<AppCompatTextView>(R.id.title_artist)!!
        val skeletonView = itemView.findViewById<ShimmerLayout>(R.id.skeleton_view)

        init {
            itemView.setOnClickListener {
                onClick(datas[adapterPosition].id!!)
                val intent = Intent(itemView.context, ArtistDetailActivity::class.java).apply {
                    putExtra("label", "Artist songs")
                    putExtra("data", datas[adapterPosition])
                }
                itemView.context.startActivity(intent)
            }
        }

    }

}