package com.pmberjaya.loopmusicandroid.app.setting

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.util.SessionManager
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.activity_shake_gesture.*

/**
 * Created by Exel staderlin on 6/20/2019.
 */
class ShakeGestureActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shake_gesture)
        setToolbar()
        initObject()
        initListener()
        switch1.isChecked = mSessionManager.switchShakeAndGesture!!
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Shake and Gesture"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
    }

    private fun initListener() {
        switch1.setOnCheckedChangeListener { buttonView, isChecked ->
            when {
                isChecked -> mSessionManager.switchShakeAndGesture = true
                else -> mSessionManager.switchShakeAndGesture = false
            }
        }

    }

}