package com.pmberjaya.loopmusicandroid.app.setting.account

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.widget.Toolbar
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.loading_layout.*

/**
 * Created by Exel staderlin on 7/6/2019.
 */
class ChangePasswordActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: AccountViewModel
    private var imageUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        setToolbar()
        initObject()
        initListener()
        initObserver()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Edit Account"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(AccountViewModel::class.java)
        imageUrl = mSessionManager.account.image.toString()
    }

    private fun initListener() {
        update_button.setOnClickListener {
            when {
                Utility.checkInternetConnection(this) -> changePassword()
                else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
            }
        }
    }

    private fun initObserver() {

        mViewModel.updateResponse.observe(this, Observer<BaseDataCallback<UserData>> {
            loading_layout.visibility = GONE
            if (it?.status!! && it.data != null) {
                mSessionManager.account = it.data!!
                Snackbar.make(
                    findViewById(android.R.id.content),
                    it.message.toString(),
                    Snackbar.LENGTH_LONG
                ).show()
            } else {
                Utility.showErrorDialog(this, it.message.toString())
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            loading_layout.visibility = GONE
        })

    }

    private fun changePassword() {
        if (Utility.blankValidator(old_pass_edit_text) &&
            Utility.blankValidator(new_pass_edit_text) &&
            Utility.blankValidator(conf_new_pass_edit_text) &&
            Utility.isPasswordValid(old_pass_edit_text) &&
            Utility.isPasswordValid(new_pass_edit_text)
        ) {
            if (new_pass_edit_text.text.toString() == conf_new_pass_edit_text.text.toString()) {
                mViewModel.changePassword("", mSessionManager.account.id, paramUpdate())
                loading_layout.visibility = VISIBLE
            } else {
                Toast.makeText(this, "Confirmation password salah", Toast.LENGTH_SHORT).show()
            }

        }
    }


    private fun paramUpdate(): HashMap<String, String> {
        val param = HashMap<String, String>()
        param["password"] = old_pass_edit_text.text.toString()
        param["new_password"] = new_pass_edit_text.text.toString()
        return param
    }

}