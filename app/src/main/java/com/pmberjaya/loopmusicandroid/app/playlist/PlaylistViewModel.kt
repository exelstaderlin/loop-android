package com.pmberjaya.loopmusicandroid.app.playlist

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.BaseRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.pmberjaya.loopmusicandroid.model.PlaylistSongData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 6/26/2019.
 */
class PlaylistViewModel : ViewModel() {
    var playlistResponse by MutableLiveDataInitialization<BaseListCallback<PlaylistData>>()
    var playlistSongResponse by MutableLiveDataInitialization<BaseListCallback<PlaylistSongData>>()
    var createPlaylistResponse by MutableLiveDataInitialization<BaseCallback>()
    var deleteResponse by MutableLiveDataInitialization<BaseCallback>()
    var editResponse by MutableLiveDataInitialization<BaseDataCallback<PlaylistData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun getPlaylist(token: String, userId: Int) {
        val observable = RestClient.getApiInterface(token).getPlaylist(userId)
        BaseDataRx<PlaylistData>().requestList(
            observable,
            object : DisposableObserver<BaseListCallback<PlaylistData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: BaseListCallback<PlaylistData>) {
                    playlistResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getPlaylistSong(token: String, id: Int) {
        val observable = RestClient.getApiInterface(token).getSongByPlaylist(id.toString())
        BaseDataRx<PlaylistSongData>().requestList(
            observable,
            object : DisposableObserver<BaseListCallback<PlaylistSongData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: BaseListCallback<PlaylistSongData>) {
                    playlistSongResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun createPlaylist(token: String, id: Int, param: HashMap<String, String>) {
        val observable = RestClient.getApiInterface(token).createPlaylist(id.toString(), param)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                createPlaylistResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun editPlaylist(token: String, id: Int, imageUrl: String) {
        val observable = RestClient.getApiInterface(token).editPlaylist(id.toString(), imageUrl)
        BaseDataRx<PlaylistData>().request(observable, object : DisposableObserver<BaseDataCallback<PlaylistData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseDataCallback<PlaylistData>) {
                editResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }


    fun deletePlaylist(token: String, id: Int) {
        val observable = RestClient.getApiInterface(token).deletePlaylist(id)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                deleteResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun deleteSongByPlaylist(token: String, playlistId: Int, songId: Int) {
        val observable = RestClient.getApiInterface(token).deleteSongByPlaylist(playlistId, songId)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                deleteResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

}