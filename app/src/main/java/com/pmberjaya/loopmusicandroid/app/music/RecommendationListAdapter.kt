package com.pmberjaya.loopmusicandroid.app.music

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.music.MusicPlayerActivity
import com.pmberjaya.loopmusicandroid.model.SongData

/**
 * Created by Exel staderlin on 6/20/2019.
 */
class RecommendationListAdapter(var datas: List<SongData>, var onClick: (songData : SongData) -> Unit) : RecyclerView.Adapter<RecommendationListAdapter.ViewHolder>() {

    private var listIdSong = ArrayList<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_list_song, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        listIdSong.add(datas[position].id)
        holder.titleSong.text = datas[position].title
        holder.titleArtist.text  = datas[position].artist?.name
        holder.duration.text  = datas[position].duration
        holder.playcount.text  = "Playcount : " + datas[position].playcount
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val titleSong = itemView.findViewById<AppCompatTextView>(R.id.title_song)!!
        val titleArtist = itemView.findViewById<AppCompatTextView>(R.id.title_artist)!!
        val duration = itemView.findViewById<AppCompatTextView>(R.id.duration)
        val playcount = itemView.findViewById<AppCompatTextView>(R.id.playcount)
        val context = itemView.context

        init {
            itemView.setOnClickListener {
                onClick(datas[adapterPosition])
            }
        }

    }

}