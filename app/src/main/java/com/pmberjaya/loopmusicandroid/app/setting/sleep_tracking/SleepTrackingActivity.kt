package com.pmberjaya.loopmusicandroid.app.setting.sleep_tracking

import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.miband.Miband
import com.pmberjaya.loopmusicandroid.miband.MibandCallback
import com.pmberjaya.loopmusicandroid.miband.listener.HeartrateListener
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.activity_sleep_tracking.*
import kotlinx.android.synthetic.main.loading_layout.*
import java.util.*


/**
 * Created by Exel staderlin on 6/20/2019.
 */
private const val SCAN_DURATION: Long = 20000 // 60s

class SleepTrackingActivity : BaseActivity() {

    private var isScanning = Scanning.SCANNING_OFF
    private var adapter: BluetoothAdapter? = null
    private val handler = Handler()
    private val stopRunnable = Runnable { stopDiscovery() }
    private val deviceData = ArrayList<DeviceData>()
    private var candidateListAdapter: DeviceCandidateAdapter? = null
    private var newLeScanCallback: ScanCallback? = null
    private var mBluetoothDevice: BluetoothDevice? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private lateinit var mSessionManager: SessionManager
    private lateinit var miband: Miband

    private val leScanCallback = BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
        Log.d("TAG :", device.name + ": " + (scanRecord?.size ?: -1))
        handleDeviceFound(device)
    }

    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == BluetoothDevice.ACTION_BOND_STATE_CHANGED) {
                try {
                    Toast.makeText(
                        this@SleepTrackingActivity,
                        "Paired with " + mBluetoothDevice?.name,
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    Log.d("Error", e.toString())
                }
            }
        }
    }

    private val heartRateNotifyListener = HeartrateListener { heartRate ->
        runOnUiThread {
//            Toast.makeText(
//                this@SleepTrackingActivity,
//                "Heart Rate : $heartRate",
//                Toast.LENGTH_SHORT
//            ).show()
        }
    }

    private val mibandCallback = object : MibandCallback {
        override fun onSuccess(data: Any?, status: Int) {
            when (status) {
                MibandCallback.STATUS_SEARCH_DEVICE -> {
                    Log.e("TAG : ", "성공: STATUS_SEARCH_DEVICE")
                    addConnectedDevices(data as BluetoothDevice)
                    miband.connect(data, this)
                }
                MibandCallback.STATUS_CONNECT -> {
                    Log.e("TAG : ", "성공: STATUS_CONNECT")
                    miband.setHeartRateScanListener(heartRateNotifyListener, this)
                }
                MibandCallback.STATUS_SEND_ALERT -> Log.e("TAG : ", "성공: STATUS_SEND_ALERT")
                MibandCallback.STATUS_START_HEARTRATE_SCAN -> Log.e("TAG : ", "성공: STATUS_START_HEARTRATE_SCAN")
            }
        }

        override fun onFail(errorCode: Int, msg: String, status: Int) {
            when (status) {
                MibandCallback.STATUS_SEARCH_DEVICE -> Log.e("TAG : ", "실패: STATUS_SEARCH_DEVICE")
                MibandCallback.STATUS_CONNECT -> Log.e("TAG : ", "실패: STATUS_CONNECT")
                MibandCallback.STATUS_SEND_ALERT -> Log.e("TAG : ", "실패: STATUS_SEND_ALERT")
                MibandCallback.STATUS_START_HEARTRATE_SCAN -> Log.e("TAG : ", "실패: STATUS_START_HEARTRATE_SCAN")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_tracking)
        Utility.checkThePermission(this)
        setToolbar()
        initListener()
        initObject()
        searchPairedDevice()
        setBroadCastReceiver()
        setAdapter()
        switch1.isChecked = mSessionManager.switchSleepTracking!!
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Sleep Tracking"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initListener() {

        switch1.setOnCheckedChangeListener { buttonView, isChecked ->
            // miband.startHeartRateScan(0, this.mibandCallback)  // Ini untuk scan heart Rate
            when {
                isChecked -> {
                    mSessionManager.switchSleepTracking = true
                    scanningDevice()
                }
                else -> {
                    mSessionManager.switchSleepTracking = false
                    stopDiscovery()
                }
            }
        }
    }

    private fun initObject() {
        try { mBluetoothAdapter = (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter } catch (e : Exception) { e.stackTrace }
        mSessionManager = SessionManager(this)
        candidateListAdapter = DeviceCandidateAdapter(this, deviceData)
    }

    private fun searchPairedDevice() {
        try {
            miband = Miband(applicationContext)
            miband.searchDevice(mBluetoothAdapter, this.mibandCallback)
            miband.setDisconnectedListener { miband.searchDevice(mBluetoothAdapter, mibandCallback) }
        } catch (e: java.lang.Exception) {
            e.stackTrace
        }
    }

    private fun setBroadCastReceiver() {
        val intentFilter = IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        intentFilter.priority = IntentFilter.SYSTEM_HIGH_PRIORITY
        registerReceiver(bluetoothReceiver, intentFilter)
    }

    private fun setAdapter() {
        list_device.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        list_device.setHasFixedSize(true)
        list_device.adapter = candidateListAdapter
    }

    private fun addConnectedDevices(device : BluetoothDevice) {
        val uuids = device.uuids
        Log.d("found device: ", device.name + ", " + device.address)
        val candidate = DeviceData(device, uuids)
        candidate.status = "Connected"
        val deviceData = ArrayList<DeviceData>()
        deviceData.add(candidate)
        list_connected_device.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        list_connected_device.setHasFixedSize(true)
        list_connected_device.adapter = DeviceCandidateAdapter(this , deviceData)
        connected_device_layout.visibility = View.VISIBLE
    }

    private fun scanningDevice() {
        if (isScanning()) {
            stopDiscovery()
        } else {
            startDiscovery(Scanning.SCANNING_BTLE)
        }
    }

    private fun isScanning(): Boolean {
        return isScanning != Scanning.SCANNING_OFF
    }

    private fun startDiscovery(what: Scanning) {
        Log.d("Starting discovery:", what.toString())
        discoveryStarted(what) // just to make sure
        when {
            ensureBluetoothReady() -> when (what) {
                Scanning.SCANNING_BT -> startBTDiscovery()
                else -> when {
                    Utility.supportsBluetoothLE(this) -> startBTLEDiscovery()
                    else -> discoveryFinished()
                }
            }
            else -> {
                discoveryFinished()
                Snackbar.make(
                    findViewById(android.R.id.content),
                    "Enable Bluetooth to discover devices",
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun startBTLEDiscovery() {
        Log.d("TAG : ", "Starting BTLE Discovery")
        handler.removeMessages(0, stopRunnable)
        handler.sendMessageDelayed(getPostMessage(stopRunnable), SCAN_DURATION)
        adapter?.startLeScan(leScanCallback)
    }

    private fun stopDiscovery() {
        Log.d("TAG :", "Stopping discovery")
        if (isScanning()) {
            val wasScanning = isScanning
            discoveryFinished()
            when (wasScanning) {
                Scanning.SCANNING_BT -> stopBTDiscovery()
                Scanning.SCANNING_BTLE -> stopBTLEDiscovery()
                else -> stopNewBTLEDiscovery()
            }
            handler.removeMessages(0, stopRunnable)
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun stopNewBTLEDiscovery() {
        if (adapter == null)
            return

        val bluetoothLeScanner = adapter?.bluetoothLeScanner
        if (bluetoothLeScanner == null) {
            Log.d("TAG :", "could not get BluetoothLeScanner()!")
            return
        }
        if (newLeScanCallback == null) {
            Log.d("TAG :", "newLeScanCallback == null!")
            return
        }
        bluetoothLeScanner.stopScan(newLeScanCallback)
    }

    private fun stopBTLEDiscovery() {
        adapter?.stopLeScan(leScanCallback)
    }

    private fun stopBTDiscovery() {
        adapter?.cancelDiscovery()
    }

    private fun ensureBluetoothReady(): Boolean {
        val available = checkBluetoothAvailable()
//        startButton.setEnabled(available)
        if (available) {
            adapter?.cancelDiscovery()
            // must not return the result of cancelDiscovery()
            // appears to return false when currently not scanning
            return true
        }
        switch1.isChecked = false
        return false
    }

    private fun checkBluetoothAvailable(): Boolean {
        val bluetoothService = getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        val adapter = bluetoothService.adapter
        if (adapter == null) {
            Log.d("TAG : ", "No bluetooth available")
            this.adapter = null
            return false
        }
        if (!adapter.isEnabled) {
            Log.d("TAG : ", "Bluetooth not enabled")
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivity(enableBtIntent)
            this.adapter = null
            return false
        }
        this.adapter = adapter
        return true
    }

    private fun discoveryStarted(what: Scanning) {
        isScanning = what
        loading_layout.visibility = View.VISIBLE
    }

    private fun discoveryFinished() {
        isScanning = Scanning.SCANNING_OFF
        loading_layout.visibility = View.GONE
    }

    private fun startBTDiscovery() {
        Log.d("TAG :", "Starting BT Discovery")
        handler.removeMessages(0, stopRunnable)
        handler.sendMessageDelayed(getPostMessage(stopRunnable), SCAN_DURATION)
        adapter?.startDiscovery()
    }

    private fun handleDeviceFound(device: BluetoothDevice) {
        val uuids = device.uuids
        Log.d("found device: ", device.name + ", " + device.address)

        val candidate = DeviceData(device, uuids)
        if (device.bondState == BluetoothDevice.BOND_BONDED) {
            candidate.status = "Previous Connected Devices"
            Log.d("Bounded device: ", device.name + ", " + device.address)
        }

        if (candidate.name != "(unknown)") {
            Log.d("TAG : ", "Recognized supported device: $candidate")
            val index = deviceData.indexOf(candidate)
            if (index >= 0) {
                deviceData[index] = candidate // replace
            } else {
                deviceData.add(candidate)
            }
            candidateListAdapter?.notifyDataSetChanged()
        }

    }

    fun startPairing(device: BluetoothDevice) {
        mBluetoothDevice = device
        device.createBond()
    }

    private fun getPostMessage(runnable: Runnable): Message {
        val m = Message.obtain(handler, runnable)
        m.obj = runnable
        return m
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(bluetoothReceiver)
    }

    private enum class Scanning {
        SCANNING_BT,
        SCANNING_BTLE,
        SCANNING_NEW_BTLE,
        SCANNING_OFF
    }


}