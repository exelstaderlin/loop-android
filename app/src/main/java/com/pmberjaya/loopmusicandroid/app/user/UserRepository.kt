package com.pmberjaya.loopmusicandroid.app.user

import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.UploadFoto
import com.pmberjaya.loopmusicandroid.model.UserData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody

@ExperimentalCoroutinesApi
class UserRepository {

    fun goLogin(param : Map<String, String>) : Flow<BaseDataCallback<UserData>> {
        return flow {
            val request = RestClient.getApiInterface("").goLogin(param)
            emit(request)
        }.flowOn(Dispatchers.IO)
    }

}