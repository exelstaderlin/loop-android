/*  Copyright (C) 2015-2019 Andreas Shimokawa, Carsten Pfeiffer, Daniele
    Gobbetti, Taavi Eomäe

    This file is part of Gadgetbridge.

    Gadgetbridge is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Gadgetbridge is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
package com.pmberjaya.loopmusicandroid.app.setting.sleep_tracking

import android.bluetooth.BluetoothDevice
import android.os.Parcel
import android.os.ParcelUuid
import android.os.Parcelable
import android.util.Log
import com.pmberjaya.loopmusicandroid.util.AndroidUtilJava
import java.lang.reflect.InvocationTargetException
import java.util.*

/**
 * A device candidate is a Bluetooth device that is not yet managed by
 * Gadgetbridge. Only if a DeviceCoordinator steps up and confirms to
 * support this candidate, will the candidate be promoted to a GBDevice.
 */
class DeviceData : Parcelable {

    val device: BluetoothDevice?
    val serviceUuids: Array<ParcelUuid>

    val macAddress: String
        get() = if (device != null) device.address else "Unknown Address"

    var status: String = ""
        get() = field
        set(value) {
            field = value
        }

    val name: String
        get() {
            var deviceName: String? = null
            try {
                val method = device!!.javaClass.getMethod("getAliasName")
                deviceName = method.invoke(device) as String
            } catch (ignore: NoSuchMethodException) {
                Log.d("TAG :", "Could not get device alias for " + device!!.name)
            } catch (ignore: IllegalAccessException) {
                Log.d("TAG :", "Could not get device alias for " + device!!.name)
            } catch (ignore: InvocationTargetException) {
                Log.d("TAG :", "Could not get device alias for " + device!!.name)
            }
            catch (e : Exception) {
                Log.d("Exception :", e.toString())
            }

            if (deviceName == null || deviceName.isEmpty()) {
                deviceName = device!!.name
            }
            if (deviceName == null || deviceName.isEmpty()) {
                deviceName = "(unknown)"
            }
            return deviceName
        }

    private fun mergeServiceUuids(
        serviceUuids: Array<ParcelUuid>?,
        deviceUuids: Array<ParcelUuid>?
    ): Array<ParcelUuid> {
        val uuids = HashSet<ParcelUuid>()
        if (serviceUuids != null) {
            uuids.addAll(Arrays.asList(*serviceUuids))
        }
        if (deviceUuids != null) {
            uuids.addAll(Arrays.asList(*deviceUuids))
        }
        return uuids.toTypedArray()
    }


    constructor(device: BluetoothDevice, serviceUuids: Array<ParcelUuid>?) {
        this.device = device
        this.serviceUuids = mergeServiceUuids(serviceUuids, device.uuids)
    }

    private constructor(`in`: Parcel) {
        device = `in`.readParcelable(javaClass.classLoader)
        if (device == null) {
            throw IllegalStateException("Unable to read state from Parcel")
        }
        val uuids = AndroidUtilJava.toParcelUuids(`in`.readParcelableArray(javaClass.classLoader))
        serviceUuids = mergeServiceUuids(uuids, device.uuids)
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeParcelable(device, 0)
        dest.writeParcelableArray(serviceUuids, 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }

        val that = o as DeviceData?
        return device!!.address == that!!.device!!.address
    }

    override fun hashCode(): Int {
        return device!!.address.hashCode() xor 37
    }

    override fun toString(): String {
        return "$name: $macAddress"
    }

    companion object CREATOR : Parcelable.Creator<DeviceData> {
        override fun createFromParcel(parcel: Parcel): DeviceData {
            return DeviceData(parcel)
        }

        override fun newArray(size: Int): Array<DeviceData?> {
            return arrayOfNulls(size)
        }
    }

}
