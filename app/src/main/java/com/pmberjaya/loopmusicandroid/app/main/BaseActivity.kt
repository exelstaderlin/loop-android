package com.pmberjaya.loopmusicandroid.app.main

import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.utilities.Utility


abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun startActivity(intent: Intent, options: Bundle?) {
        super.startActivity(intent, options)
        onStartNewActivity()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        onLeaveThisActivity()
    }

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        onStartNewActivity()
    }

    override fun startService(service: Intent?): ComponentName? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            super.startForegroundService(service)
        else
            super.startService(service)
    }

    private fun onLeaveThisActivity() {
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
    }

    private fun onStartNewActivity() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>, observer: Observer<T>) {
        liveData.observe(this, observer)
        liveData.observe(this, Observer {
            if (it is HttpException) {
                httpExceptionHandling(it)
            } else {
                Utility.showErrorDialog(this, it.toString())
            }
        })
    }

    fun <T : Throwable> defaultErrorMessageHandling(liveData: MutableLiveData<T>) {
        liveData.observe(this, Observer<T> {
            it?.printStackTrace()
            if (it is HttpException) {
                httpExceptionHandling(it)
            } else {
                Utility.showErrorDialog(this, it.toString())
            }
        })
    }

    private fun httpExceptionHandling(e: HttpException) {
        when (e.code()) {
            400 -> Toast.makeText(this, "Bad Request", Toast.LENGTH_SHORT).show()
//            401 -> showLoginDialog()
            403 -> Utility.showErrorDialog(this, getString(R.string.error_access_right))
            404 -> Utility.showErrorDialog(this, getString(R.string.error_page_doesnt_exist))
            408 -> Utility.showErrorDialog(this, getString(R.string.error_rto))
            410 -> {
//                showErrorDialog(this, getString(R.string.page_no_longer_available)) {
                //                    val intent = Intent(this, SwitchProjectActivity::class.java).apply {
//                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
//                    }
//                    startActivity(intent)
//                    finish()
//                }
            }
            500 -> Utility.showErrorDialog(this, getString(R.string.error_internal_server))
            else -> {
                Utility.showErrorDialog(
                    this,
                    getString(R.string.error_submit_problem) + " " + getString(R.string.support_link)
                )
            }
        }
    }
}