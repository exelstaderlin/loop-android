package com.pmberjaya.loopmusicandroid.app.setting.equalizer

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color.parseColor
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.service.MusicService
import com.pmberjaya.loopmusicandroid.service.ServicesManager
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_equalizer.*
import kotlinx.android.synthetic.main.fragment_equalizer.*


/**
 * Created by Exel staderlin on 6/20/2019.
 */
class EqualizerActivity : BaseActivity() {

    lateinit var mSessionManager: SessionManager
    lateinit var mViewModel: EqualizerViewModel

    private var musicConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MusicService.MusicBinder
            binderEqualizer(binder)
        }

        override fun onServiceDisconnected(name: ComponentName) {}
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val eqValueStr = mSessionManager.equalizerCustom!!
        addEqualizer(eqValueStr)
        Log.d("Equalizer String", eqValueStr)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_equalizer)
        setToolbar()
        initObject()
        initListener()
        initObserver()
        checkRunningMusic()
        getEqualizer()
//        equalizer_switch.isChecked = mSessionManager.switchEqualizer!!
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Equalizer"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(EqualizerViewModel::class.java)
    }

    private fun initListener() {
        equalizer_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            when {
                isChecked -> mSessionManager.switchEqualizer = true
                else -> mSessionManager.switchEqualizer = false
            }
        }
    }

    private fun initObserver() {
        mViewModel.eqResponse.observe(this, Observer {
            if(it?.status!!)
                mSessionManager.equalizerCustom = it.data?.frequency
        })
    }

    private fun getEqualizer() {
        when {
            Utility.checkInternetConnection(this) -> mViewModel.getEqualizer("", mSessionManager.account.id)
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun addEqualizer(frequency: String) {
        when {
            Utility.checkInternetConnection(this) -> mViewModel.addEqualizer("", mSessionManager.account.id, frequency)
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun binderEqualizer(binder: MusicService.MusicBinder) {
        val sessionId = binder.service.audioSessionId
        val equalizerFragment = EqualizerFragment.newBuilder()
            .setAccentColor(parseColor("#4caf50"))
            .setAudioSessionId(sessionId)
            .build()
        supportFragmentManager.beginTransaction()
            .replace(R.id.eqFrame, equalizerFragment)
            .commit()
    }

    private fun checkRunningMusic() {
        if (ServicesManager.getRunningService()) {
            val intent = Intent(this, MusicService::class.java).apply {
                putExtra("url", mSessionManager.playingSong.linkUrl)
                if (ServicesManager.isPlaying)
                    putExtra("audio", "resume")
                else
                    putExtra("audio", "pause")
            }
            startService(intent)
            bindService(intent, musicConnection, Context.BIND_AUTO_CREATE)
        } else {
            Toast.makeText(this, "Musik harus di jalankan", Toast.LENGTH_SHORT).show()
        }
    }

}