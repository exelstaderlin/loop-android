package com.pmberjaya.loopmusicandroid.app.user

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_forget_password.*
import kotlinx.android.synthetic.main.activity_forget_password.email_edit_text
import kotlinx.android.synthetic.main.loading_layout.*

class ForgotPasswordActivity : BaseActivity() {
    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: ResetPasswordViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        initObject()
        initListener()
        initObserver()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)
    }

    private fun initListener() {
        btn_reset.setOnClickListener {
            resetPassword()
        }
    }

    private fun initObserver() {
        mViewModel.resetResponse.observe(this, Observer<BaseCallback> {
            loading_layout.visibility = GONE
            when {
                it?.status!! -> {
                    Toast.makeText(this, it.message , Toast.LENGTH_LONG).show()
                }
                else -> Utility.showErrorDialog(this, it.message!!)
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            loading_layout.visibility = GONE
        })

    }


    private fun resetPassword() {
        when {
            Utility.checkInternetConnection(this) -> {
                if (Utility.blankValidator(email_edit_text) &&
                    Utility.isEmailValid(email_edit_text)
                ) {
                    mViewModel.resetPassword("", email_edit_text.text.toString())
                    loading_layout.visibility = View.VISIBLE
                }
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }
}