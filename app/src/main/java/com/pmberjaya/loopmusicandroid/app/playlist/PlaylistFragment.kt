package com.pmberjaya.loopmusicandroid.app.playlist

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.graphics.Color
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.*
import android.widget.TextView
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.artist.ArtistAdapter
import com.pmberjaya.loopmusicandroid.app.artist.ArtistViewModel
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainActivity
import com.pmberjaya.loopmusicandroid.app.main.MainViewModel
import com.pmberjaya.loopmusicandroid.model.ArtistData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.fragment_playlist.*
import kotlinx.android.synthetic.main.loading_layout.*

/**
 * Created by Exel staderlin on 6/20/2019.
 */
class PlaylistFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mArtistViewModel: ArtistViewModel
    private lateinit var mPlaylistViewModel: PlaylistViewModel
    private lateinit var mPlaylistAdapter: PlaylistAdapter
    private lateinit var mArtistAdapter: ArtistAdapter
    private lateinit var mMainViewModel: MainViewModel
    private var nameAddPlaylist = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_playlist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObject()
        initObserver()
        setAdapterArtist()
        setAdapterPlaylist()
    }

    private fun initObject() {
        swipe_refresh_layout.setOnRefreshListener(this)
        mSessionManager = SessionManager(activity as Activity)
        mArtistViewModel = ViewModelProvider(this).get(ArtistViewModel::class.java)
        mPlaylistViewModel = ViewModelProvider(this).get(PlaylistViewModel::class.java)
        mMainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mPlaylistAdapter = PlaylistAdapter(this, emptyList())
        mArtistAdapter = ArtistAdapter(List(3) { ArtistData() }) { /*Nothing*/ }
    }

    private fun initObserver() {
        mArtistViewModel.artistResponse.observe(this, Observer {
            swipe_refresh_layout.isRefreshing = false
            when {
                it?.status!! && it.data.size != 0 -> {
                    mArtistAdapter.datas = it.data
                    mArtistAdapter.notifyDataSetChanged()
                }
                else -> Utility.showErrorDialog(activity as Activity, it.message!!)
            }

        })

        mPlaylistViewModel.playlistResponse.observe(this, Observer {
            swipe_refresh_layout.isRefreshing = false
            when {
                it?.status!! && it.data.size != 0 -> {
                    mPlaylistAdapter.datas = it.data
                    mPlaylistAdapter.notifyDataSetChanged()
                }
            }

        })

        mPlaylistViewModel.createPlaylistResponse.observe(this, Observer {
            loading_layout.visibility = View.GONE
            if (it?.status!!) {
                swipe_refresh_layout.isRefreshing = true
                getPlaylist()
            }
            Snackbar.make(
                activity!!.findViewById(android.R.id.content),
                it.message.toString(),
                Snackbar.LENGTH_LONG
            )
                .show()
        })

        mPlaylistViewModel.deleteResponse.observe(this, Observer {
            if (it?.status!!) {
                swipe_refresh_layout.isRefreshing = true
                getPlaylist()
            }
            Snackbar.make(
                activity!!.findViewById(android.R.id.content),
                it.message.toString(),
                Snackbar.LENGTH_LONG
            )
                .show()
        })

        mMainViewModel.uploadAvatarResponse.observe(this, Observer {
            when {
                it!!.status -> createPlaylist(nameAddPlaylist, it.data?.photoPath!!)
                else -> Toast.makeText(
                    activity as Activity,
                    "Upload failed",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        (activity as BaseActivity).defaultErrorMessageHandling(
            mArtistViewModel.errorResponse,
            Observer {
                loading_layout.visibility = View.GONE
                swipe_refresh_layout.isRefreshing = false
            })

    }

    private fun getAllArtist() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mArtistViewModel.getAllArtist("")
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun getPlaylist() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mPlaylistViewModel.getPlaylist("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun createPlaylist(playlistName: String, imageUrl: String) {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mPlaylistViewModel.createPlaylist(
                    "",
                    mSessionManager.account.id,
                    paramMap(playlistName, imageUrl)
                )
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun deletePlaylist(id: Int) {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mPlaylistViewModel.deletePlaylist("", id)
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun paramMap(playlistName: String, imageUrl: String): HashMap<String, String> {
        val map = HashMap<String, String>()
        map["title"] = playlistName
        map["image"] = imageUrl
        return map
    }

    private fun setAdapterArtist() {
        artist_recycler_view.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        artist_recycler_view.setHasFixedSize(true)
        artist_recycler_view.adapter = mArtistAdapter
    }

    private fun setAdapterPlaylist() {
        playlist_recycler_view.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        playlist_recycler_view.setHasFixedSize(true)
        playlist_recycler_view.adapter = mPlaylistAdapter
    }

    fun showDialogCreatePlaylist() {
        (activity as MainActivity).playlistAddDialog?.init { name, imagePath, imageName ->
            this.nameAddPlaylist = name
            loading_layout.visibility = View.VISIBLE
            if (imagePath != "" || imageName != "") {
                uploadImage(imagePath, imageName)
            } else {
                createPlaylist(
                    nameAddPlaylist,
                    "https://cdn.dribbble.com/users/29051/screenshots/2515769/icon.png"
                )
            }
        }
    }

    private fun uploadImage(imagePath: String, imageName: String) {
        when {
            Utility.checkInternetConnection(activity as Activity) -> mMainViewModel.uploadImage(
                Utility.requestMultiPartBody(
                    imagePath,
                    imageName
                )!!
            )
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }


    fun deleteDialog(itemView: View, id: Int) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(itemView.context)
        val v = LayoutInflater.from(itemView.context).inflate(R.layout.spinner_item, null)
        val text1 = v.findViewById(R.id.text1) as TextView
        builder.setView(v)
        dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val layoutParams = dialog.window!!.attributes
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.gravity = Gravity.BOTTOM
        text1.text = "DELETE"
        text1.setTextColor(Color.BLACK)
        text1.setOnClickListener {
            deletePlaylist(id)
            dialog.dismiss()
        }

        dialog.show()

    }

    override fun onRefresh() {
        getAllArtist()
        getPlaylist()
    }

    override fun onResume() {
        super.onResume()
        getAllArtist()
        getPlaylist()
    }
}