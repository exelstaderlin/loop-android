package com.pmberjaya.loopmusicandroid.app.user

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.io.BaseRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import io.reactivex.observers.DisposableObserver

class ResetPasswordViewModel : ViewModel() {

    var resetResponse by MutableLiveDataInitialization<BaseCallback>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun resetPassword(token: String, email : String) {
        val observable = RestClient.getApiInterface(token).resetPassword(email)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>(){
            override fun onComplete() {
            }

            override fun onNext(t: BaseCallback) {
                resetResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }


}