package com.pmberjaya.loopmusicandroid.app.setting.account

import androidx.lifecycle.ViewModel

import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.UserData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 7/19/2019.
 */
class AccountViewModel : ViewModel() {

    var updateResponse by MutableLiveDataInitialization<BaseDataCallback<UserData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun updateProfile(token: String, id: Int, param: HashMap<String, String>) {
        val observable = RestClient.getApiInterface(token).updateProfile(id, param)
        BaseDataRx<UserData>().request(observable, object : DisposableObserver<BaseDataCallback<UserData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseDataCallback<UserData>) {
                updateResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun changePassword(token: String, id: Int, param: HashMap<String, String>) {
        val observable = RestClient.getApiInterface(token).changePassword(id, param)
        BaseDataRx<UserData>().request(observable, object : DisposableObserver<BaseDataCallback<UserData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseDataCallback<UserData>) {
                updateResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

}