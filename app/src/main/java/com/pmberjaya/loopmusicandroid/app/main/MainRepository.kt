package com.pmberjaya.loopmusicandroid.app.main

import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.UploadFoto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody

@ExperimentalCoroutinesApi
class MainRepository {

    fun uploadFoto(requestBody: MultipartBody) : Flow<BaseDataCallback<UploadFoto>> {
        return flow {
            val request = RestClient.getApiSariputta().uploadAvatar(requestBody)
            emit(request)
        }.flowOn(Dispatchers.IO)
    }
}