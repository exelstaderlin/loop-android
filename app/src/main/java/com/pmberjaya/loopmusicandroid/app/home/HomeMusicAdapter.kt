package com.pmberjaya.loopmusicandroid.app.home

import android.content.Intent
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.music.MusicPlayerActivity
import com.pmberjaya.loopmusicandroid.model.SongData
import com.squareup.picasso.Picasso
import io.supercharge.shimmerlayout.ShimmerLayout


class HomeMusicAdapter(var datas: List<SongData>) : Adapter<HomeMusicAdapter.HomeViewHolder>() {
    private var listIdSong = ArrayList<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_music_play, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return when {
            datas.size > 3 -> 3
            else -> datas.size
        }
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        when(datas[position].id) {
            -1 -> {
                holder.skeletonView.startShimmerAnimation()
                holder.titleArtist.background = holder.context.getDrawable(R.color.colorPrimaryDark)
                holder.titleSong.background  = holder.context.getDrawable(R.color.colorPrimaryDark)
            }
            else -> {
                holder.skeletonView.stopShimmerAnimation()
                holder.titleArtist.background = holder.context.getDrawable(R.color.transparent)
                holder.titleSong.background  = holder.context.getDrawable(R.color.transparent)
                listIdSong.add(datas[position].id)
            }
        }
        holder.titleSong.text = datas[position].title
        holder.titleArtist.text = datas[position].artist?.name
        Picasso.get()
            .load(datas[position].artist?.image)
            .placeholder(R.drawable.music_image)
            .error(R.drawable.music_image)
            .into(holder.cardViewImage)
    }

    inner class HomeViewHolder(itemView: View) : ViewHolder(itemView) {
        val cardViewImage = itemView.findViewById<AppCompatImageView>(R.id.image)
        val titleSong = itemView.findViewById<AppCompatTextView>(R.id.title_song)
        val titleArtist = itemView.findViewById<AppCompatTextView>(R.id.title_artist)
        val skeletonView = itemView.findViewById<ShimmerLayout>(R.id.skeleton_view)
        val context = itemView.context

        init {
            itemView.setOnClickListener {
                val intent = Intent(context, MusicPlayerActivity::class.java).apply {
                    putExtra("data", datas[adapterPosition])
                    putExtra("from", "adapter")
                    putIntegerArrayListExtra("list_song_id", listIdSong)
                    putParcelableArrayListExtra("list_song_temp", datas as ArrayList)
                    putExtra("position", adapterPosition)
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                }
                context.startActivity(intent)
            }
        }
    }


}