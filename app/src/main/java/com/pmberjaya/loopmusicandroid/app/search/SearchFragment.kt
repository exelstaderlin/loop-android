package com.pmberjaya.loopmusicandroid.app.search

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.artist.ArtistAdapter
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.model.ArtistData
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * Created by Exel staderlin on 6/20/2019.
 */
class SearchFragment : Fragment() {

    private lateinit var mViewModel: SearchViewModel
    private lateinit var mSessionManager: SessionManager
    var timer: CountDownTimer? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObject()
        initListener()
        initObserver()
        getRecentlySearch()
    }

    private fun initObject() {
        mSessionManager = SessionManager(activity as Activity)
        mViewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
    }

    private fun initListener() {
        search_autocomplete_textview.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                timer?.cancel()
                if ((s ?: "").isNotBlank()) {
                    val keyword = s!!.trimStart().trimEnd().toString()
                    timer = createTimer(600) {  // setelah mengetik tunggu waktu 600 mls untuk request API
                        search_progress_bar.visibility = View.VISIBLE
                        searchSong(keyword)
                    }.start()
                } else {
                    search_progress_bar.visibility = View.GONE
                    artist_recycle_view.visibility = View.GONE
                    playlist_recycle_view.visibility = View.GONE
                    getRecentlySearch()
                }
            }
        })

    }

    private fun initObserver() {
        mViewModel.recentlySearch.observe(this, Observer {
            val listSong = ArrayList<SongData>()//init array utk song
            val listArtist = ArrayList<ArtistData>()//init array utk artist
            val listPlaylist = ArrayList<PlaylistData>() //init array utk playlist
            if (it?.status!! && it.data.size != 0) {
                label.text = "Recently Search"
                for (i in 0 until it.data.size) {
                    when (it.data[i].songs) {
                        null -> {
                        }
                        else -> listSong.add(it.data[i].songs!!) //add lagu ke dlm list
                    }
                    when (it.data[i].artist) {
                        null -> labelArtist.visibility = View.GONE
                        else -> {
                            listArtist.add(it.data[i].artist!!) //add artist ke dlm list
                        }
                    }
                    when (it.data[i].playlist) {
                        null -> labelPlaylist.visibility = View.GONE
                        else -> {
                            labelPlaylist.visibility = View.VISIBLE
                            listPlaylist.add(it.data[i].playlist!!) //add playlist ke dlm list
                        }
                    }
                }
            }
            setAdapterSong(listSong)//tampilin isinya
            setAdapterArtist(listArtist)//tampilin isinya
            setAdapterPlaylist(listPlaylist)//tampilin isinya
        })

        mViewModel.searchResponse.observe(this, Observer {
            search_progress_bar.visibility = View.GONE
            if (it?.status!! && it.data.size != 0) {
                label.text = "Result"
                setAdapterSong(it.data)
            } else {
                setAdapterSong(emptyList())
            }
        })

        mViewModel.artistResponse.observe(this, Observer {
            search_progress_bar.visibility = View.GONE
            if (it?.status!! && it.data.size != 0) {
                labelArtist.visibility = View.VISIBLE
                artist_recycle_view.visibility = View.VISIBLE
                label.text = "Result"
                setAdapterArtist(it.data)
            } else {
                labelArtist.visibility = View.GONE
                artist_recycle_view.visibility = View.GONE
                setAdapterArtist(emptyList())
            }
        })

        mViewModel.playlistResponse.observe(this, Observer {
            search_progress_bar.visibility = View.GONE
            if (it?.status!! && it.data.size != 0) {
                labelPlaylist.visibility = View.VISIBLE
                playlist_recycle_view.visibility = View.VISIBLE
                label.text = "Result"
                setAdapterPlaylist(it.data)
            } else {
                labelPlaylist.visibility = View.GONE
                playlist_recycle_view.visibility = View.GONE
                setAdapterPlaylist(emptyList())
            }
        })

        (activity as BaseActivity).defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            search_progress_bar.visibility = View.GONE
        })
    }

    private fun createTimer(interval: Long, func: () -> Unit): CountDownTimer {
        return object : CountDownTimer(interval, 10) {
            override fun onFinish() {
                func()
            }

            override fun onTick(millisUntilFinished: Long) {
            }
        }
    }

    private fun getRecentlySearch() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.getRecentlySearch("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(activity as Activity, getString(R.string.no_internet_found))
        }
    }

    private fun searchSong(keyword: String) {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.searchSong("", keyword)
                mViewModel.searchArtist("", keyword)
                mViewModel.searchPlaylist("", mSessionManager.account.id.toString() ,keyword)
            }
            else -> Utility.showErrorDialog(activity as Activity, getString(R.string.no_internet_found))
        }
    }

    private fun postSearchLog(id: Int, category: String) {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.postSearchLog("", param(id, category))
            }
            else -> Utility.showErrorDialog(activity as Activity, getString(R.string.no_internet_found))
        }
    }

    private fun param(id: Int, category: String): HashMap<String, Int> {
        val map = HashMap<String, Int>()
        map["id_user"] = mSessionManager.account.id
        when (category) {
            "song" -> map["id_song"] = id
            "artist" -> map["id_artist"] = id
            else -> map["id_playlist"] = id
        }
        return map
    }

    private fun setAdapterSong(data: List<SongData>) {
        song_recycle_view.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        song_recycle_view.setHasFixedSize(true)
        song_recycle_view.adapter = SongListAdapter(data) { id -> postSearchLog(id, "song") }
    }

    private fun setAdapterArtist(data: List<ArtistData>) {
        artist_recycle_view.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        artist_recycle_view.setHasFixedSize(true)
        artist_recycle_view.adapter = ArtistAdapter(data) { id -> postSearchLog(id, "artist") }
    }

    private fun setAdapterPlaylist(data: List<PlaylistData>) {
        playlist_recycle_view.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        playlist_recycle_view.setHasFixedSize(true)
        playlist_recycle_view.adapter = PlaylistAdapterSearch(data) { id -> postSearchLog(id, "playlist") }
    }

}