package com.pmberjaya.loopmusicandroid.app.music

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.*
import android.media.MediaPlayer
import android.os.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.widget.SeekBar
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainViewModel
import com.pmberjaya.loopmusicandroid.model.RecommendationData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.sensoric.SensorBuilder
import com.pmberjaya.loopmusicandroid.sensoric.SensorConfig
import com.pmberjaya.loopmusicandroid.sensoric.ShakeDetector
import com.pmberjaya.loopmusicandroid.sensoric.WaveDetector
import com.pmberjaya.loopmusicandroid.service.MusicService
import com.pmberjaya.loopmusicandroid.service.ServicesManager
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.toolbar
import kotlinx.android.synthetic.main.activity_music_player.*


/**
 * Created by Exel staderlin on 6/21/2019.
 */

//const val url = "http://dl3.wapkizfile.info/ddl/60386b599691fd2b67db3a2e2f087101/matikiri+wapkiz+com/Shawn%20Mendes%20Camila%20Cabello%20-%20Senorita%20(%20cover%20by%20J.Fla%20)-(matikiri.wapkiz.com).mp3"

const val url = "http://10.0.0.51:8080/api/play/song/01%20-%20Melody"
const val CHANNEL_BROADCAST = "musicService"
//const val url =
//    "http://dl.lagu123.link/download/rre0IS0hmHersrAK85e1RQ/1566209284/a80b1d8cceb90aebad8a959213a0fde9/3a03e396b8eea9e2703f4aada761bf8a.mp3?s=2&name=Joji%20-%20SLOW%20DANCING%20IN%20THE%20DARK"

class MusicPlayerActivity : BaseActivity(), ShakeDetector.ShakeListener, WaveDetector.WaveListener {

    lateinit var musicService: MusicService
    private lateinit var mData: SongData
    private lateinit var mSessionManager: SessionManager
    private lateinit var recommendationBottomSheet: BottomSheetBehavior<NestedScrollView>
    private lateinit var mHandler: Handler
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var mViewModel: MainViewModel
    private lateinit var from: String
    private lateinit var listSongId: ArrayList<Int>
    private lateinit var listSongTemp: ArrayList<SongData>
    private lateinit var mSensorShake: SensorBuilder
    private lateinit var mSensorWave: SensorBuilder
    private var position: Int = 0

    override fun onBackPressed() {
        mSensorShake.unRegisterShakeListener()
        mSensorWave.unRegisterWaveListener()
        finish()
        super.onBackPressed()
    }

    private val updateMusicTime = object : Runnable {
        override fun run() {
            try {
                seek_bar!!.max = musicService.duration
                seek_bar!!.progress = musicService.currentPosition
                val durationStr = Utility.milliSecondsToTimer(musicService.currentPosition.toLong())
                val audioLengthStr = Utility.milliSecondsToTimer(musicService.duration.toLong())
                duration_progress_text!!.text = durationStr
                duration_final_text!!.text = audioLengthStr
                mHandler.postDelayed(this, 100)
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        }
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {  // get called when music is finish or completed
            if (intent.action == CHANNEL_BROADCAST) {
                try {
                    recommendationBottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
                    seek_bar!!.progress = 0
                    countDownTimer(5000).start()
                } catch (e: Exception) {
                    Log.d("Error", e.toString())
                }
            }
        }
    }

    private var musicConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MusicService.MusicBinder
            musicService = binder.service
        }

        override fun onServiceDisconnected(name: ComponentName) {}
    }

    override fun onShakeDetected(message: String) {
        if (mSessionManager.switchShakeAndGesture!!) {
            if (!SensorConfig.shake!!) {
                Log.d("OnShake  ", "Shake")
                SensorConfig.shake = true
                if (Utility.checkThePermission(this)) {
                    when (message) {
                        "kiri" -> {
                            prevOnClick()
                        }
                        "kanan" -> {
                            nextOnClick()
                        }
                    }
                }
            }
        }
    }

    override fun onShakeStopped() {
        SensorConfig.shake = false
    }

    var count = 0
    override fun onWave() {
        count++
        println("COUNT = " + count)
        val handler = Handler()
        handler.postDelayed({
            if (mSessionManager.switchShakeAndGesture!!) {
                Log.d("OnWave  ", "WAVAVAVAEVEVEVEVE")
                when {
                    ServicesManager.isPlaying -> pauseAudio()
                    else -> resumeAudio()
                }
            }
        }, 100)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player)
        setToolbar()
        setBroadCastReceiver()
        initObject()
        initListener()
        initObserver()
        initViewData()
        checkPermitMp()
        setAdapterSong()
        mSensorShake.setShakeListener(listener = this)
        mSensorWave.setWaveListener(listener = this)


    }

    private fun initObject() {
        mSensorShake = SensorBuilder(this)
        mSensorWave = SensorBuilder(this)
        musicService = MusicService()
        mediaPlayer = MediaPlayer()
        mData = intent.getParcelableExtra("data")
        from = intent.getStringExtra("from")
        listSongId = intent.getIntegerArrayListExtra("list_song_id")
        listSongTemp = intent.getParcelableArrayListExtra("list_song_temp")
        position = intent.getIntExtra("position", 0)
        mHandler = Handler()
        mSessionManager = SessionManager(this)
        recommendationBottomSheet = BottomSheetBehavior.from(bottom_sheet_recommendation)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private fun initListener() {
        play_btn.setOnClickListener {
            when {
                ServicesManager.isPlaying -> pauseAudio()
                else -> resumeAudio()
            }
        }

        next_btn.setOnClickListener {
            nextOnClick()
        }

        previous_btn.setOnClickListener {
            prevOnClick()
        }

        seek_bar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (b) musicService.seekTo(i)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                mHandler.removeCallbacks(updateMusicTime)
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                mHandler.removeCallbacks(updateMusicTime)
                val currentPosition =
                    Utility.progressToTimer(seekBar.progress, musicService.duration)
                mediaPlayer.seekTo(currentPosition)
                updateProgressBar()
            }
        })
    }

    private fun initObserver() {
        AsyncTask.execute {
            mViewModel.songDataResponse.observe(this, Observer {
                if (it?.status!! && it.data != null) {
                    mData = it.data!!
                    initViewData()
                    newSong()
                }
                else {
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private fun initViewData() {
        Picasso.get()
            .load(mData.artist?.image)
            .error(R.drawable.music_image)
            .into(circleImageView)
        title_song.text = mData.title
        title_artist.text = mData.artist?.name
        println(mSessionManager.listSongId)
    }

    private fun checkPermitMp() {
        when (from) {
            "adapter" -> {
                when (mData.id) {
                    mSessionManager.playingSong.id -> presentSong()
                    else -> newSong()
                }
            }
            else -> presentSong()
        }
    }

    private fun newSong() {
        mSessionManager.positionListSongId = this.position
        mSessionManager.listSongId = this.listSongId
        mSessionManager.listSongTemp = this.listSongTemp
        mSessionManager.playingSong = mData
        val r = Runnable {
            //            playAudio(url)
            playAudio(mData.linkUrl ?: "")
        }
        mHandler.postDelayed(r, 500)
    }

    private fun presentSong() {
        if (ServicesManager.getRunningService()) {
            when (ServicesManager.isPlaying) {
                true -> play_btn!!.setImageResource(R.drawable.ic_play)
                else -> play_btn!!.setImageResource(R.drawable.ic_pause)
            }
            val intent = Intent(this, MusicService::class.java).apply {
                putExtra("url", mSessionManager.playingSong.linkUrl)
                putExtra("audio", "resume")
            }
            bindService(intent, musicConnection, 0)
            updateProgressBar()
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Music"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun setBroadCastReceiver() {
        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        val intent = IntentFilter()
        intent.addAction(CHANNEL_BROADCAST)
        localBroadcastManager.registerReceiver(broadcastReceiver, intent)
    }

    private fun updateProgressBar() {
        mHandler.postDelayed(updateMusicTime, 10)
    }

    private fun playAudio(url: String) {
        play_btn!!.setImageResource(R.drawable.ic_play)
        ServicesManager.isPlaying = true
        Log.d("Play", ">>> $url")
        val intent = Intent(this, MusicService::class.java).apply {
            putExtra("url", url)
            putExtra("audio", "play")
        }
        startService(intent)
        bindService(intent, musicConnection, Context.BIND_AUTO_CREATE)
        updateProgressBar()
    }

    private fun pauseAudio() {
        Toast.makeText(this, "Paused song", Toast.LENGTH_SHORT).show()
        play_btn!!.setImageResource(R.drawable.ic_pause)
        ServicesManager.isPlaying = false
        val intent = Intent(this, MusicService::class.java).apply {
            putExtra("audio", "pause")
        }
        startService(intent)
    }

    private fun resumeAudio() {
        Toast.makeText(this, "Resumed song", Toast.LENGTH_SHORT).show()
        play_btn!!.setImageResource(R.drawable.ic_play)
        ServicesManager.isPlaying = true
        val intent = Intent(this, MusicService::class.java).apply {
            putExtra("audio", "resume")
        }
        mediaPlayer.seekTo(musicService.currentPosition)
        startService(intent)
    }

    private fun nextOnClick() {
//        if (position != listSongId.size - 1) {
//            SensorConfig.shake = true
//            val idSong = listSongId[position + 1]
//            nextOrPrevSong(idSong) //Next Song
//            Toast.makeText(this, "Next song", Toast.LENGTH_SHORT).show()
//            position++
//        } else {
//            SensorConfig.shake = false
//            Toast.makeText(this, "This is your last song, Can't Next", Toast.LENGTH_SHORT).show()
//        }

        if (position != listSongTemp.size - 1) {
            SensorConfig.shake = true
            val song = listSongTemp[position + 1]
            mData = song
            initViewData()
            newSong()  //Next Song
            Toast.makeText(this, "Next song", Toast.LENGTH_SHORT).show()
            position++
        } else {
            SensorConfig.shake = false
            Toast.makeText(this, "This is your last song, Can't Next", Toast.LENGTH_SHORT).show()
        }
    }

    private fun prevOnClick() {
//        if (position != 0) {
//            SensorConfig.shake = true
//            val idSong = listSongId[position - 1]
//            nextOrPrevSong(idSong) //Previous Song
//            Toast.makeText(this, "Prev song", Toast.LENGTH_SHORT).show()
//            position--
//        } else {
//            SensorConfig.shake = false
//            Toast.makeText(this, "You at the top of the list song, Can't Prev", Toast.LENGTH_SHORT)
//                .show()
//        }

        if (position != 0) {
            SensorConfig.shake = true
            val song = listSongTemp[position - 1]
            mData = song
            initViewData()
            newSong()  //Next Song
            Toast.makeText(this, "Prev song", Toast.LENGTH_SHORT).show()
            position--
        } else {
            SensorConfig.shake = false
            Toast.makeText(this, "You at the top of the list song, Can't Prev", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun nextOrPrevSong(idSong: Int) {
        when {
            Utility.checkInternetConnection(this) -> mViewModel.getSongById("", idSong)
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun listSongData(listRecommendation: ArrayList<RecommendationData>): List<SongData> {
        val listSongData = ArrayList<SongData>()
        for (i in 0 until listRecommendation.size) {
            listSongData.add(listRecommendation[i].songs ?: SongData())
        }
        return listSongData
    }

    private fun setAdapterSong() {
        val listSongData = listSongData(mSessionManager.listSongRecommendationData)
        song_list_recycler_view.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        song_list_recycler_view.setHasFixedSize(true)
        song_list_recycler_view.adapter = MusicRecommendationListAdapter(listSongData) {
            this.mData = it
            initViewData()
            newSong()
            recommendationBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun countDownTimer(millisInFuture: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                Log.d("seconds remaining: ", (millisUntilFinished / 1000).toString())
            }

            override fun onFinish() {
                nextOnClick()
                recommendationBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN
            }
        }
    }


}