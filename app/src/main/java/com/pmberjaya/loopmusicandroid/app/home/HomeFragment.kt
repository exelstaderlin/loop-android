package com.pmberjaya.loopmusicandroid.app.home

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.model.LogUserData
import com.pmberjaya.loopmusicandroid.model.RecommendationData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * Created by Exel staderlin on 6/17/2019.
 */
class HomeFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private lateinit var mRecentlyAdapter: HomeMusicAdapter
    private lateinit var mRecommendedAdapter: HomeMusicAdapter
    private lateinit var mPopularAdapter: HomeMusicAdapter
    private lateinit var mViewModel: HomeViewModel
    private lateinit var mSessionManager: SessionManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObject()
        initListener()
        initObserver()
        getRecommendationSong()
        getRecentlyPlayedSong()
        getPopularSong()
        setAdapterRecently()
        setAdapterRecommended()
        setAdapterTopPopular()
    }

    private fun initObject() {
        mRecentlyAdapter = HomeMusicAdapter(List(3) { SongData() })
        mRecommendedAdapter = HomeMusicAdapter(List(3) { SongData() })
        mPopularAdapter = HomeMusicAdapter(List(3) { SongData() })
        mViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        mSessionManager = SessionManager(activity as Activity)
    }

    private fun initListener() {
        swipe_refresh_layout.setOnRefreshListener(this)
        see_all_recently.setOnClickListener {
            val intent = Intent(activity, SongListActivity::class.java).apply {
                putExtra("label", "Recently played")
                putParcelableArrayListExtra("data", mRecentlyAdapter.datas as ArrayList)
            }
            activity?.startActivity(intent)
        }

        see_all_recommended.setOnClickListener {
            val intent = Intent(activity, SongListActivity::class.java).apply {
                putExtra("label", "Recommended for you")
                putParcelableArrayListExtra("data", mRecommendedAdapter.datas as ArrayList)
            }
            activity?.startActivity(intent)
        }

        see_all_top_popular.setOnClickListener {
            val intent = Intent(activity, SongListActivity::class.java).apply {
                putExtra("label", "Top Popular Song")
                putParcelableArrayListExtra("data", mPopularAdapter.datas as ArrayList)
            }
            activity?.startActivity(intent)
        }
    }

    private fun initObserver() {
        mViewModel.recommendationResponse.observe(this, Observer {
            swipe_refresh_layout.isRefreshing = false
            when {
                it?.status!! && it.data.size != 0 -> {
                    home_recommend_label.visibility = VISIBLE
                    recommend_recycler_view.visibility = VISIBLE
                    see_all_recommended.visibility = VISIBLE
                    mSessionManager.listSongRecommendationData = it.data
                    mRecommendedAdapter.datas = getSongFromRecommendation(it.data)
                    mRecommendedAdapter.notifyDataSetChanged()
                }
                else -> {
                    home_recommend_label.visibility = GONE
                    recommend_recycler_view.visibility = GONE
                    see_all_recommended.visibility = GONE
//                    Utility.showErrorDialog(activity as Activity, it.message!!)
                }
            }
        })

        mViewModel.recentlyResponse.observe(this, Observer {
            swipe_refresh_layout.isRefreshing = false
            when {
                it?.status!! && it.data.size != 0 -> {
                    home_recently_label.visibility = VISIBLE
                    recently_recycler_view.visibility = VISIBLE
                    see_all_recently.visibility = VISIBLE
                    mRecentlyAdapter.datas = getSongFromLog(it.data)
                    mRecentlyAdapter.notifyDataSetChanged()
                }
                else -> {
                    home_recently_label.visibility = GONE
                    recently_recycler_view.visibility = GONE
                    see_all_recently.visibility = GONE
//                    Utility.showErrorDialog(activity as Activity, it.message!!)
                }
            }
        })

        mViewModel.popResponse.observe(this, Observer {
            swipe_refresh_layout.isRefreshing = false
            when {
                it?.status!! && it.data.size != 0 -> {
                    mPopularAdapter.datas = getSongFromLog(it.data)
                    mPopularAdapter.notifyDataSetChanged()
                }
                else -> Utility.showErrorDialog(activity as Activity, it.message!!)
            }
        })

        (activity as BaseActivity).defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            swipe_refresh_layout.isRefreshing = false
        })
    }

    private fun getSongFromRecommendation(recommendationData: List<RecommendationData>): ArrayList<SongData> {
        val songs = ArrayList<SongData>()
        for (i in recommendationData.indices) {
            var songData = SongData()
            if (recommendationData[i].songs != null) {
                songData = recommendationData[i].songs!!
            }
            songs.add(songData)
        }
        return songs
    }

    private fun getSongFromLog(log: List<LogUserData>): ArrayList<SongData> {
        val songs = ArrayList<SongData>()
        for (i in log.indices) {
            if (log[i].songs != null)
                songs.add(log[i].songs!!)
        }
        return songs
    }

    private fun setAdapterRecently() {
        recently_recycler_view.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        recently_recycler_view.setHasFixedSize(true)
        recently_recycler_view.adapter = mRecentlyAdapter
    }

    private fun setAdapterRecommended() {
        recommend_recycler_view.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        recommend_recycler_view.setHasFixedSize(true)
        recommend_recycler_view.adapter = mRecommendedAdapter
    }

    private fun setAdapterTopPopular() {
        top_popular_recycler_view.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        top_popular_recycler_view.setHasFixedSize(true)
        top_popular_recycler_view.adapter = mPopularAdapter
    }


    private fun getRecommendationSong() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.getRecommendationSong("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun getRecentlyPlayedSong() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.getRecentlyPlayedSong("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }

    private fun getPopularSong() {
        when {
            Utility.checkInternetConnection(activity as Activity) -> {
                mViewModel.getPopularSong("")
            }
            else -> Utility.showErrorDialog(
                activity as Activity,
                getString(R.string.no_internet_found)
            )
        }
    }


    override fun onRefresh() {
        getRecommendationSong()
        getRecentlyPlayedSong()
        getPopularSong()
    }

}