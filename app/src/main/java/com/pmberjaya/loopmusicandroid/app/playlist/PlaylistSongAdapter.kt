package com.pmberjaya.loopmusicandroid.app.playlist

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import android.view.*
import android.widget.TextView
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.music.MusicPlayerActivity
import com.pmberjaya.loopmusicandroid.model.PlaylistSongData
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.utilities.Utility

/**
 * Created by Exel staderlin on 7/6/2019.
 */

class PlaylistSongAdapter(var activity: PlaylistDetailActivity, var datas: List<PlaylistSongData>) :
    RecyclerView.Adapter<PlaylistSongAdapter.ViewHolder>() {

    private var listIdSong = ArrayList<Int>()

    private var listSongData = ArrayList<SongData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_list_song, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        listIdSong.add(datas[position].songs?.id!!)
        listSongData.add(datas[position].songs!!)
        Utility.setPrecomputedText(holder.titleSong, datas[position].songs?.title ?: "")
        Utility.setPrecomputedText(holder.titleArtist, datas[position].songs?.artist?.name ?: "")
        Utility.setPrecomputedText(holder.duration, datas[position].songs?.duration ?: "")
        Utility.setPrecomputedText(holder.playcount, datas[position].songs?.playcount ?: "")
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleSong = itemView.findViewById<AppCompatTextView>(R.id.title_song)!!
        val titleArtist = itemView.findViewById<AppCompatTextView>(R.id.title_artist)!!
        val duration = itemView.findViewById<AppCompatTextView>(R.id.duration)
        val playcount = itemView.findViewById<AppCompatTextView>(R.id.playcount)

        init {
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, MusicPlayerActivity::class.java).apply {
                    putExtra("data", datas[adapterPosition].songs)
                    putExtra("from", "adapter")
                    putIntegerArrayListExtra("list_song_id", listIdSong)
                    putParcelableArrayListExtra("list_song_temp", listSongData)
                    putExtra("position", adapterPosition)
                }
                itemView.context.startActivity(intent)
            }
            itemView.setOnLongClickListener {
                deleteDialog(itemView, datas[adapterPosition].songs?.id!!)
                false
            }
        }

        private fun deleteDialog(itemView: View, id: Int) {
            val dialog: AlertDialog
            val builder = AlertDialog.Builder(itemView.context)
            val v = LayoutInflater.from(itemView.context).inflate(R.layout.spinner_item, null)
            val text1 = v.findViewById(R.id.text1) as TextView
            builder.setView(v)
            dialog = builder.create()
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val layoutParams = dialog.window!!.attributes
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.gravity = Gravity.BOTTOM
            text1.text = "DELETE"
            text1.setTextColor(Color.BLACK)
            text1.setOnClickListener {
                activity.deleteSongByPlaylist(id)
                dialog.dismiss()
            }

            dialog.show()

        }

    }
}