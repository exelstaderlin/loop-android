package com.pmberjaya.loopmusicandroid.app.user

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainActivity
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.Config
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.loading_layout.*

/**
 * Created by Exel staderlin on 6/13/2019.
 */
class RegisterActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initObject()
        initListener()
        initObserver()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
    }

    private fun initListener() {
        register_button.setOnClickListener {
            if (
                Utility.blankValidator(email_edit_text) &&
                Utility.blankValidator(password_edit_text) &&
                Utility.blankValidator(name_edit_text) &&
                Utility.isEmailValid(email_edit_text) &&
                Utility.isPasswordValid(password_edit_text)
            ) {
                mViewModel.goRegister("", paramLogin())
                loading_layout.visibility = VISIBLE

            }
        }
    }


    private fun initObserver() {

        mViewModel.registerResponse.observe(this, Observer<BaseDataCallback<UserData>> {
            loading_layout.visibility = GONE
            when {
                it?.status!! && it.data != null -> {
                    mSessionManager.account = it.data!!
                    mSessionManager.hasSession = true
                    intentToMain()
                }
                else -> Utility.showErrorDialog(this, it.message!!)
            }
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            loading_layout.visibility = GONE
        })
    }


    private fun intentToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        Toast.makeText(this, getString(R.string.login_success), Toast.LENGTH_LONG).show()
        finishAffinity()
    }

    private fun paramLogin(): HashMap<String, String> {
        val param = HashMap<String, String>()
        param["email"] = email_edit_text.text.toString()
        param["name"] = name_edit_text.text.toString()
        param["password"] = password_edit_text.text.toString()
        param["image"] = Config.DEFAULT_PROFILE_PIC_URL
        param["role"] = "0"
        return param
    }


}