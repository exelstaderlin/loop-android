package com.pmberjaya.loopmusicandroid.app.playlist

import android.content.Intent
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.model.PlaylistData
import com.squareup.picasso.Picasso

class PlaylistAdapter(var fragment: PlaylistFragment, var datas: List<PlaylistData>) :
    Adapter<PlaylistAdapter.HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.card_playlist, parent, false)
        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return datas.size + 1
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        Log.d("position", position.toString())
        if (position == 0) {
            holder.titlePlaylist.text = "Create Your Playlist"
            holder.cardViewImage.setImageResource(R.drawable.ic_add_box)
        } else {
            holder.titlePlaylist.text = datas[position - 1].title
            holder.titleCreator.text = "Created by " + "Exel staderlin"
            Picasso.get()
                .load(datas[position - 1].image)
                .placeholder(R.drawable.music_image)
                .error(R.drawable.music_image)
                .into(holder.cardViewImage)
        }
    }

    inner class HomeViewHolder(itemView: View) : ViewHolder(itemView) {
        val cardViewImage = itemView.findViewById<AppCompatImageView>(R.id.image)
        val titlePlaylist = itemView.findViewById<AppCompatTextView>(R.id.title_playlist)
        val titleCreator = itemView.findViewById<AppCompatTextView>(R.id.title_creator)

        init {
            itemView.setOnClickListener {
                when (adapterPosition) {
                    0 -> fragment.showDialogCreatePlaylist()
                    else -> {
                        val intent = Intent(itemView.context, PlaylistDetailActivity::class.java).apply {
                            putExtra("label", "Your songs")
                            putExtra("data", datas[adapterPosition - 1])
                        }
                        itemView.context.startActivity(intent)
                    }
                }
            }

            itemView.setOnLongClickListener {
                fragment.deleteDialog(itemView,  datas[adapterPosition - 1].id!!)
                false
            }
        }
    }


}