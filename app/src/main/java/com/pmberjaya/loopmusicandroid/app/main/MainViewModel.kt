package com.pmberjaya.loopmusicandroid.app.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.model.UploadFoto
import com.pmberjaya.loopmusicandroid.util.singleArgViewModelFactory
import io.reactivex.observers.DisposableObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody

@ExperimentalCoroutinesApi
class MainViewModel() : ViewModel() {

    var uploadAvatarResponse by MutableLiveDataInitialization<BaseDataCallback<UploadFoto>>()
    var songDataResponse by MutableLiveDataInitialization<BaseDataCallback<SongData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

//    companion object {
//        val FACTORY = singleArgViewModelFactory(::MainViewModel)
//    }


    fun uploadImage(requestBody: MultipartBody) {
//        viewModelScope.launch {
//            repository.uploadFoto(requestBody)
//                .catch { throwable -> errorResponse.value = throwable }
//                .collect { data -> uploadAvatarResponse.value = data }
//        }

//            val apiInterface = RestClient.getApiSariputta().uploadAvatar(requestBody)
//            BaseDataRx<UploadFoto>().request(
//                apiInterface,
//                object : DisposableObserver<BaseDataCallback<UploadFoto>>() {
//                    override fun onComplete() {}
//                    override fun onNext(t: BaseDataCallback<UploadFoto>) {
//                        uploadAvatarResponse.value = t
//                    }
//
//                    override fun onError(e: Throwable) {
//                        errorResponse.value = e
//                    }
//                })
    }

    fun getSongById(token: String, id: Int) {
        val apiInterface = RestClient.getApiInterface(token).getSongById(id)
        BaseDataRx<SongData>().request(
            apiInterface,
            object : DisposableObserver<BaseDataCallback<SongData>>() {
                override fun onComplete() {}
                override fun onNext(t: BaseDataCallback<SongData>) {
                    songDataResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }


}