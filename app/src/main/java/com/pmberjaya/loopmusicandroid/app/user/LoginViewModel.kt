package com.pmberjaya.loopmusicandroid.app.user

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.pmberjaya.loopmusicandroid.callback.BaseDataCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.UserData
import com.pmberjaya.loopmusicandroid.util.singleArgViewModelFactory
import io.reactivex.observers.DisposableObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class LoginViewModel(private val repository: UserRepository) : ViewModel() {

    var loginResponse by MutableLiveDataInitialization<BaseDataCallback<UserData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    companion object {
        val FACTORY = singleArgViewModelFactory(::LoginViewModel)
    }

    fun goLogin(param: HashMap<String, String>) {
        viewModelScope.launch {
            repository.goLogin(param)
                .catch { throwable ->
                    errorResponse.value = throwable
                    Log.d("throwable : ", throwable.toString())
                }
                .collect { data -> loginResponse.value = data }
        }
    }

}