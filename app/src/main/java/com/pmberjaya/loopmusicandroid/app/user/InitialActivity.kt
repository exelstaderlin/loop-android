package com.pmberjaya.loopmusicandroid.app.user
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.widget.EditText
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.main.MainActivity
import com.pmberjaya.loopmusicandroid.io.Config
import com.pmberjaya.loopmusicandroid.util.SessionManager
import kotlinx.android.synthetic.main.activity_auth.*

class InitialActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        Config.setAppUrl(this)
        initObject()
        setupView()
        initListener()
        skipLogin(checkSession())
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
    }


    private fun setupView() {
    }

    private fun initListener() {
        btn_login.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        btn_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        loop_logo.setOnClickListener {
            dialogUbahIpAddress()
        }
    }

    private fun checkSession() : Boolean{
        return if (mSessionManager.hasSession) {
            true
        } else {
            mSessionManager.clearSession()
            false
        }
    }

    private fun skipLogin(isLogin: Boolean) {
        if (isLogin) {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }
    }

    private fun dialogUbahIpAddress(){

        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.layout_custom_ip_address, null)
        val local_ip = alertLayout.findViewById<EditText>(R.id.local_ip)

        val alert = AlertDialog.Builder(this)
        alert.setView(alertLayout)
        alert.setCancelable(false)
        alert.setNegativeButton("Cancel") { _, _ ->  }
        alert.setPositiveButton("Done") { _, _ ->
            val localIp = local_ip.text.toString()
            Log.d("param",localIp)
            mSessionManager.baseUrl = localIp
            Config.setAppUrl(this)
        }
        val dialog = alert.create()
        dialog.show()
    }


}
