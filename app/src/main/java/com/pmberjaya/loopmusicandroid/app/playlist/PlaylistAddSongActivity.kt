package com.pmberjaya.loopmusicandroid.app.playlist

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.CountDownTimer
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.search.SearchViewModel
import com.pmberjaya.loopmusicandroid.model.SongData
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.activity_playlist_add_song.*

/**
 * Created by Exel staderlin on 7/6/2019.
 */
class PlaylistAddSongActivity : BaseActivity() {

    private lateinit var mAdapter: AddSongListAdapter
    private lateinit var mViewModel: SearchViewModel
    private lateinit var mPlaylistViewModel: PlaylistViewModel
    private lateinit var mSessionManager: SessionManager
    private var playlistId: Int = 0
    var timer: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_add_song)
        setToolbar()
        initObject()
        initListener()
        initObserver()
        setAdapterRecently()
        getPlaylist()
        getRecentlySearch()
    }


    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "Add song"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }


    private fun initObject() {
        mAdapter = AddSongListAdapter(emptyList())
        playlistId = intent.getIntExtra("id_playlist", 0)
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        mPlaylistViewModel = ViewModelProvider(this).get(PlaylistViewModel::class.java)
    }

    private fun initListener() {
        search_autocomplete_textview.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                timer?.cancel()
                if ((s ?: "").isNotBlank()) {
                    val keyword = s!!.trimStart().trimEnd().toString()
                    timer = createTimer(600) {
                        search_progress_bar.visibility = View.VISIBLE
                        searchSong(keyword)
                    }.start()
                } else {
                    search_progress_bar.visibility = View.GONE
                }
            }
        })

        search_autocomplete_textview.setOnFocusChangeListener { v, hasFocus ->
        }

    }


    private fun initObserver() {
        mViewModel.recentlySearch.observe(this, Observer {
            val listSong = ArrayList<SongData>()//init array utk song
            if (it?.status!! && it.data.size != 0) {
                label.text = "Recently Search"
                for (i in 0 until it.data.size) {
                    when (it.data[i].songs) {
                        null -> { }
                        else -> listSong.add(it.data[i].songs!!) //add lagu ke dlm list
                    }
                }
            }
            mAdapter.datas = listSong
            mAdapter.notifyDataSetChanged()
        })


        mViewModel.searchResponse.observe(this, Observer {
            search_progress_bar.visibility = View.GONE
            if (it?.status!! && it.data.size != 0) {
                mAdapter.datas = it.data
                mAdapter.notifyDataSetChanged()
            } else {
                mAdapter.datas = List(0) { SongData() }
                mAdapter.notifyDataSetChanged()
                Snackbar.make(
                    findViewById(android.R.id.content),
                    it.message.toString(),
                    Snackbar.LENGTH_LONG
                ).show()
            }
        })

        mViewModel.addedResponse.observe(this, Observer {
            Snackbar.make(
                findViewById(android.R.id.content),
                it?.message.toString(),
                Snackbar.LENGTH_LONG
            ).show()
        })

        defaultErrorMessageHandling(mViewModel.errorResponse, Observer {
            search_progress_bar.visibility = View.GONE
        })
    }


    private fun createTimer(interval: Long, func: () -> Unit): CountDownTimer {
        return object : CountDownTimer(interval, 10) {
            override fun onFinish() {
                func()
            }

            override fun onTick(millisUntilFinished: Long) {
            }
        }
    }

    private fun getPlaylist() {
        when {
            Utility.checkInternetConnection(this) -> {
                mPlaylistViewModel.getPlaylist("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    private fun getRecentlySearch() {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.getRecentlySearch("", mSessionManager.account.id)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }



    private fun searchSong(keyword: String) {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.searchSong("", keyword)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

    fun addSongToPlaylist(songId: Int) {
        when {
            Utility.checkInternetConnection(this) -> {
                mViewModel.addSongToPlaylist("", playlistId, songId)
            }
            else -> Utility.showErrorDialog(this, getString(R.string.no_internet_found))
        }
    }

//    fun showPlaylistDialog(songId: Int) {
//        if (playlistData.isNotEmpty()) {
//            val customListDialog = CustomListDialog(this, "Your Playlist", playlistData)
//            customListDialog.show(onClick = { playlistId ->
//                addSongToPlaylist(playlistId, songId)
//            })
//        }
//    }

    private fun setAdapterRecently() {
        search_recycle_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        search_recycle_view.setHasFixedSize(true)
        search_recycle_view.adapter = mAdapter
    }

}