package com.pmberjaya.loopmusicandroid.app.playlist

import android.graphics.Bitmap
import androidx.fragment.app.FragmentActivity
import androidx.appcompat.app.AlertDialog
import android.widget.TextView
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.MainActivity
import com.pmberjaya.loopmusicandroid.views.RoundCornerImageView

class PlaylistAddDialog(var activity: FragmentActivity) {

    private var imagePath = ""
    private var imageName = ""
    private val inflater = activity.layoutInflater
    private val dialogView = inflater.inflate(R.layout.playlist_add_dialog, null)
    private val image = dialogView.findViewById<RoundCornerImageView>(R.id.image)
    private val ok = dialogView.findViewById<TextView>(R.id.positive_button)
    private val name = dialogView.findViewById<TextView>(R.id.name)
    private val builder = AlertDialog.Builder(activity).setView(dialogView)
    private val dialog = builder.create()

    fun init(yes: (name: String, imagePath : String, imageName : String) -> Unit) {
        dialog.apply {
            show()
            setCancelable(true)
            setOnCancelListener {
                dialog.dismiss()
            }
        }
        image.setOnClickListener {
            (activity as MainActivity).selectImage()
        }
        ok.setOnClickListener {
            yes(name.text.toString(), imagePath, imageName)
            dialog.dismiss()
        }
    }

    fun setImage(bitmap : Bitmap) {
        image.setImageBitmap(bitmap)
    }

    fun setImageToUpload(imagePath : String, imageName : String) {
        this.imagePath = imagePath
        this.imageName = imageName
    }

}