package com.pmberjaya.loopmusicandroid.app.home

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.appcompat.widget.Toolbar
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.main.BaseActivity
import com.pmberjaya.loopmusicandroid.app.search.SongListAdapter
import com.pmberjaya.loopmusicandroid.model.SongData
import kotlinx.android.synthetic.main.activity_song_list.*

/**
 * Created by Exel staderlin on 6/21/2019.
 */
class SongListActivity : BaseActivity() {

    private var label = ""
    private lateinit var data : List<SongData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song_list)
        setToolbar()
        initObject()
        setAdapterSong()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.title = "List Song"
        (toolbar as Toolbar).setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun initObject() {
        label = intent.getStringExtra("label")
        data = intent.getParcelableArrayListExtra("data")
        title_label.text = label
    }


    private fun setAdapterSong() {
        song_list_recycler_view.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        song_list_recycler_view.setHasFixedSize(true)
        song_list_recycler_view.adapter = SongListAdapter(data){/*Nothing*/}
    }

}