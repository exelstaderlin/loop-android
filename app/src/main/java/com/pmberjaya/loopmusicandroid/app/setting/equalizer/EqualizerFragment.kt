package com.pmberjaya.loopmusicandroid.app.setting.equalizer


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.media.audiofx.Equalizer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.db.chart.model.LineSet
import com.db.chart.view.AxisController
import com.db.chart.view.ChartView
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.model.EqualizerModel
import com.pmberjaya.loopmusicandroid.util.EqualizerSettings
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import kotlinx.android.synthetic.main.fragment_equalizer.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class EqualizerFragment : Fragment() {

    lateinit var dataset: LineSet
    lateinit var paint: Paint
    lateinit var points: FloatArray
    lateinit var mEqualizer: Equalizer
    lateinit var mSessionManager: SessionManager
    var seekBarFinal = arrayOfNulls<SeekBar>(5)
    private var audioSessionId: Int = 0
    internal var ctx: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ctx = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_equalizer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObject()
        initListener()
        initFrequencyBand(view)
        equalizeSound()
        initLineChart()
    }

    private fun initObject() {
        mEqualizer = Equalizer(0, audioSessionId)
        mSessionManager = SessionManager(ctx!!)
        paint = Paint()
        dataset = LineSet()
        EqualizerSettings.equalizerModel = EqualizerModel()
        mEqualizer.enabled = true
    }

    private fun initListener() {
        equalizer_switch.isChecked = true

        equalizer_switch.setOnCheckedChangeListener { _, isChecked ->
            mEqualizer.enabled = isChecked
        }

        spinner_dropdown_icon.setOnClickListener { equalizer_preset_spinner.performClick() }
    }

    @SuppressLint("SetTextI18n")
    private fun initFrequencyBand(view: View) {
        val numberOfFrequencyBands = 5
        points = FloatArray(numberOfFrequencyBands)
        val lowerEqualizerBandLevel = mEqualizer.bandLevelRange[0]
        val upperEqualizerBandLevel = mEqualizer.bandLevelRange[1]
        for (i in 0 until numberOfFrequencyBands) {
            val frequencyHeaderTextView = TextView(context)
            frequencyHeaderTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            frequencyHeaderTextView.gravity = Gravity.CENTER_HORIZONTAL
            frequencyHeaderTextView.setTextColor(Color.parseColor("#FFFFFF"))
            frequencyHeaderTextView.text = (mEqualizer.getCenterFreq(i.toShort()) / 1000).toString() + "Hz"

            val seekBarRowLayout = LinearLayout(context)
            seekBarRowLayout.orientation = LinearLayout.VERTICAL

            val lowerEqualizerBandLevelTextView = TextView(context)
            lowerEqualizerBandLevelTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            lowerEqualizerBandLevelTextView.setTextColor(Color.parseColor("#FFFFFF"))
            lowerEqualizerBandLevelTextView.text = (lowerEqualizerBandLevel / 100).toString() + "dB"

            val upperEqualizerBandLevelTextView = TextView(context)
            lowerEqualizerBandLevelTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            upperEqualizerBandLevelTextView.setTextColor(Color.parseColor("#FFFFFF"))
            upperEqualizerBandLevelTextView.text = (upperEqualizerBandLevel / 100).toString() + "dB"

            val layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams.weight = 1f

            var seekBar = SeekBar(context)
            var textView = TextView(context)
            when (i) {
                0 -> {
                    seekBar = view.findViewById(R.id.seekBar1)
                    textView = view.findViewById(R.id.textView1)
                }
                1 -> {
                    seekBar = view.findViewById(R.id.seekBar2)
                    textView = view.findViewById(R.id.textView2)
                }
                2 -> {
                    seekBar = view.findViewById(R.id.seekBar3)
                    textView = view.findViewById(R.id.textView3)
                }
                3 -> {
                    seekBar = view.findViewById(R.id.seekBar4)
                    textView = view.findViewById(R.id.textView4)
                }
                4 -> {
                    seekBar = view.findViewById(R.id.seekBar5)
                    textView = view.findViewById(R.id.textView5)
                }
            }
            seekBarFinal[i] = seekBar
            seekBar.progressDrawable.colorFilter = PorterDuffColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_IN)
            seekBar.thumb.colorFilter = PorterDuffColorFilter(themeColor, PorterDuff.Mode.SRC_IN)
            seekBar.id = i
            //            seekBar.setLayoutParams(layoutParams);
            seekBar.max = upperEqualizerBandLevel - lowerEqualizerBandLevel

            textView.text = frequencyHeaderTextView.text
            textView.setTextColor(Color.WHITE)
            textView.textAlignment = View.TEXT_ALIGNMENT_CENTER

            if (EqualizerSettings.isEqualizerReloaded) {
                points[i] = (EqualizerSettings.seekbarpos[i] - lowerEqualizerBandLevel).toFloat()
                dataset.addPoint(frequencyHeaderTextView.text.toString(), points[i])
                seekBar.progress = EqualizerSettings.seekbarpos[i] - lowerEqualizerBandLevel
            } else {
                points[i] = (mEqualizer.getBandLevel(i.toShort()) - lowerEqualizerBandLevel).toFloat()
                dataset.addPoint(frequencyHeaderTextView.text.toString(), points[i])
                seekBar.progress = mEqualizer.getBandLevel(i.toShort()) - lowerEqualizerBandLevel
                EqualizerSettings.seekbarpos[i] = mEqualizer.getBandLevel(i.toShort()).toInt()
                EqualizerSettings.isEqualizerReloaded = true
            }
            seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    mEqualizer.setBandLevel(i.toShort(), (progress + lowerEqualizerBandLevel).toShort())
                    points[seekBar.id] = (mEqualizer.getBandLevel(i.toShort()) - lowerEqualizerBandLevel).toFloat()
                    EqualizerSettings.seekbarpos[seekBar.id] = progress + lowerEqualizerBandLevel
                    EqualizerSettings.equalizerModel!!.seekbarpos[seekBar.id] = progress + lowerEqualizerBandLevel
                    dataset.updateValues(points)
                    lineChart.notifyDataUpdate()
                    if(equalizer_preset_spinner.selectedItem.toString() == "Custom")
                        mSessionManager.equalizerCustom = Utility.intArrToStr(EqualizerSettings.seekbarpos)

                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
                    equalizer_preset_spinner.setSelection(0)
                    EqualizerSettings.presetPos = 0
                    EqualizerSettings.equalizerModel!!.presetPos = 0
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {
                }
            })
        }
    }

    private fun equalizeSound() {
        val equalizerPresetNames = ArrayList<String>()
        val equalizerPresetSpinnerAdapter = ArrayAdapter(
            ctx!!,
            R.layout.spinner_item,
            equalizerPresetNames
        )
        equalizerPresetSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        equalizerPresetNames.add("Custom")

        for (i in 0 until mEqualizer.numberOfPresets) {
            equalizerPresetNames.add(mEqualizer.getPresetName(i.toShort()))
        }

        equalizer_preset_spinner.adapter = equalizerPresetSpinnerAdapter
        if (EqualizerSettings.isEqualizerReloaded && EqualizerSettings.presetPos != 0) {
            equalizer_preset_spinner.setSelection(EqualizerSettings.presetPos)
        }

        equalizer_preset_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                try {
                    val numberOfFreqBands: Short = 5
                    val lowerEqualizerBandLevel = mEqualizer.bandLevelRange[0]
                    if (position != 0) {
                        mEqualizer.usePreset((position - 1).toShort())
                        EqualizerSettings.presetPos = position
                        for (i in 0 until numberOfFreqBands) {
                            val frequencyValue = mEqualizer.getBandLevel(i.toShort())
                            seekBarFinal[i]?.progress = frequencyValue - lowerEqualizerBandLevel
                            points[i] = (frequencyValue - lowerEqualizerBandLevel).toFloat()
                            EqualizerSettings.seekbarpos[i] = frequencyValue.toInt()
                            EqualizerSettings.equalizerModel!!.seekbarpos[i] = frequencyValue.toInt()
                        }
                    } else {
                        EqualizerSettings.presetPos = position
                        val listEqValue = getEqValueArr(mSessionManager.equalizerCustom!!)
                        for (i in 0 until numberOfFreqBands) {
                            val frequencyValue = listEqValue[i]
                            seekBarFinal[i]?.progress = frequencyValue - lowerEqualizerBandLevel
                            points[i] = (frequencyValue - lowerEqualizerBandLevel).toFloat()
                            EqualizerSettings.seekbarpos[i] = frequencyValue
                            EqualizerSettings.equalizerModel!!.seekbarpos[i] = frequencyValue
                        }
                    }
                    dataset.updateValues(points)
                    lineChart.notifyDataUpdate()
                    val eqIntArr = EqualizerSettings.seekbarpos
                    val eqValueStr = Utility.intArrToStr(eqIntArr)
                    Toast.makeText(ctx, eqValueStr, Toast.LENGTH_SHORT).show()

                } catch (e: Exception) {
                    Toast.makeText(ctx, "Error while updating Equalizer", Toast.LENGTH_SHORT).show()
                }

                EqualizerSettings.equalizerModel!!.presetPos = position
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

    private fun initLineChart() {
        paint.color = Color.parseColor("#555555")
        paint.strokeWidth = (1.10 * EqualizerSettings.ratio).toFloat()
        dataset.color = themeColor
        dataset.isSmooth = true
        dataset.thickness = 5f
        lineChart.setXAxis(false)
        lineChart.setYAxis(false)
        lineChart.setYLabels(AxisController.LabelPosition.NONE)
        lineChart.setXLabels(AxisController.LabelPosition.NONE)
        lineChart.setGrid(ChartView.GridType.NONE, 7, 10, paint)
        lineChart.setAxisBorderValues(-300, 3300)
        lineChart.addData(dataset)
        lineChart.show()
    }

    private fun getEqValueArr(eqValueStr : String) : ArrayList<Int> {
        val exampleIntArr = ArrayList<Int>()
        var tempStr = ""
        for(i in eqValueStr.indices) {
            val value = eqValueStr[i].toString()
            if(value != ",")
                tempStr += value
            else{
                exampleIntArr.add(tempStr.toInt())
                tempStr = ""
            }
        }
        return exampleIntArr
    }

    class Builder {
        private var id = -1

        fun setAudioSessionId(id: Int): Builder {
            this.id = id
            return this
        }

        fun setAccentColor(color: Int): Builder {
            themeColor = color
            return this
        }

        fun build(): EqualizerFragment {
            return newInstance(id)
        }
    }

    companion object {

        internal var themeColor = Color.parseColor("#B24242")

        fun newInstance(audioSessionId: Int): EqualizerFragment {

            val args = Bundle()

            val fragment = EqualizerFragment()
            fragment.audioSessionId = audioSessionId
            fragment.arguments = args
            return fragment
        }

        fun newBuilder(): Builder {
            return Builder()
        }
    }

}
