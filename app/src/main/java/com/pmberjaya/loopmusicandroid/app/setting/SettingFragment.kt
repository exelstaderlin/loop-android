package com.pmberjaya.loopmusicandroid.app.setting

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.user.InitialActivity
import com.pmberjaya.loopmusicandroid.app.setting.account.EditAccountActivity
import com.pmberjaya.loopmusicandroid.app.setting.equalizer.EqualizerActivity
import com.pmberjaya.loopmusicandroid.app.setting.sleep_tracking.SleepTrackingActivity
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_setting.*


class SettingFragment : Fragment() {

    private lateinit var mSessionManager: SessionManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_setting, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObject()
        initListener()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this.context!!)
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        Picasso.get().load(mSessionManager.account.image).error(R.color.colorPrimary).into(image_circle)
        name_text_view.text = mSessionManager.account.name
        email_text_view.text = mSessionManager.account.email
        logged_in.text = "You're logged in as " +  mSessionManager.account.name
    }

    private fun initListener() {

        profile.setOnClickListener {
            val intent = Intent(activity, EditAccountActivity::class.java)
            goIntent(intent)
        }

        shake_gesture.setOnClickListener {
            val intent = Intent(activity, ShakeGestureActivity::class.java)
            goIntent(intent)
        }

        sleep_tracking.setOnClickListener {
            val intent = Intent(activity, SleepTrackingActivity::class.java)
            goIntent(intent)
        }

        equalizer.setOnClickListener {
            val intent = Intent(activity, EqualizerActivity::class.java)
            goIntent(intent)
        }

        logout.setOnClickListener {
            signOut()
        }

    }

    private fun signOut() {
        Utility.showAlertDialog(activity as Activity, "Logout", "Are you sure want to logout ?", yes = {
            mSessionManager.clearSession()
            val intent = Intent(activity, InitialActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        }, no = { /* Do nothing */ })
    }

    private fun goIntent(intent: Intent) {
        activity?.startActivity(intent)
//        activity?.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left)
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

}