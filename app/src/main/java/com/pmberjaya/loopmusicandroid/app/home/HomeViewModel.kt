package com.pmberjaya.loopmusicandroid.app.home

import androidx.lifecycle.ViewModel
import com.pmberjaya.loopmusicandroid.callback.BaseListCallback
import com.pmberjaya.loopmusicandroid.io.BaseDataRx
import com.pmberjaya.loopmusicandroid.io.MutableLiveDataInitialization
import com.pmberjaya.loopmusicandroid.io.RestClient
import com.pmberjaya.loopmusicandroid.model.LogUserData
import com.pmberjaya.loopmusicandroid.model.RecommendationData
import com.pmberjaya.loopmusicandroid.model.SongData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 6/26/2019.
 */
class HomeViewModel : ViewModel() {

    var popResponse by MutableLiveDataInitialization<BaseListCallback<LogUserData>>()
    var recentlyResponse by MutableLiveDataInitialization<BaseListCallback<LogUserData>>()
    var songResponse by MutableLiveDataInitialization<BaseListCallback<SongData>>()
    var recommendationResponse by MutableLiveDataInitialization<BaseListCallback<RecommendationData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()

    fun getRecommendationSong(token: String, userId: Int) {
        val observable = RestClient.getApiInterface(token).getRecommendationSong(userId)
        BaseDataRx<RecommendationData>().requestList(observable, object : DisposableObserver<BaseListCallback<RecommendationData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<RecommendationData>) {
                recommendationResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun getRecentlyPlayedSong(token: String, userId: Int) {
        val observable = RestClient.getApiInterface(token).getRecentlyPlayedSong(userId)
        BaseDataRx<LogUserData>().requestList(observable, object : DisposableObserver<BaseListCallback<LogUserData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<LogUserData>) {
                recentlyResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }

    fun getPopularSong(token: String) {
        val observable = RestClient.getApiInterface(token).getPopularSong()
        BaseDataRx<LogUserData>().requestList(observable, object : DisposableObserver<BaseListCallback<LogUserData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<LogUserData>) {
                popResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })
    }


    fun getSongByArtist(token: String, id: String) {
        val observable = RestClient.getApiInterface(token).getSongByArtist(id)
        BaseDataRx<SongData>().requestList(observable, object : DisposableObserver<BaseListCallback<SongData>>() {
            override fun onComplete() {
            }

            override fun onNext(t: BaseListCallback<SongData>) {
                songResponse.value = t
            }

            override fun onError(e: Throwable) {
                errorResponse.value = e
            }
        })

    }


}