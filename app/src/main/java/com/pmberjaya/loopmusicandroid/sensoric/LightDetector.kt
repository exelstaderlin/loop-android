package com.pmberjaya.loopmusicandroid.sensoric

import android.hardware.Sensor.TYPE_LIGHT
import android.hardware.SensorEvent

/**
 * Created by Exel staderlin on 3/19/2019.
 */
class LightDetector(var lightListener: LightListener) : SensorDetector(TYPE_LIGHT){

    interface LightListener {
        fun onLight(lightValue : Float)
    }

    override fun onSensorEvent(sensorEvent: SensorEvent) {
        val lightValue = sensorEvent.values[0]
        lightListener.onLight(lightValue)
    }

}