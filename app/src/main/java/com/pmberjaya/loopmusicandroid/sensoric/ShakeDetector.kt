package com.pmberjaya.loopmusicandroid.sensoric

import android.hardware.Sensor.TYPE_ACCELEROMETER
import android.hardware.SensorEvent
import android.hardware.SensorManager
import android.util.Log

/**
 * Created by Exel staderlin on 3/19/2019.
 */
class ShakeDetector(
    private var threshold: Float, private var timeBeforeDeclaringShakeStopped: Long,
    private var shakeListener: ShakeListener
) : SensorDetector(TYPE_ACCELEROMETER) {

    private var isShaking = false
    private var lastTimeShakeDetected = System.currentTimeMillis()
    private var mAccel: Float = 0.toFloat()
    private var mAccelCurrent = SensorManager.GRAVITY_EARTH

    interface ShakeListener {
        fun onShakeDetected(message: String)
        fun onShakeStopped()
    }

    override fun onSensorEvent(sensorEvent: SensorEvent) {
        // Shake detection
        val x = sensorEvent.values[0]
        val y = sensorEvent.values[1]
        val z = sensorEvent.values[2]
        val mAccelLast = mAccelCurrent
        mAccelCurrent = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
        val delta = mAccelCurrent - mAccelLast
        mAccel = mAccel * 0.9f + delta
        if (mAccel > threshold) {
            lastTimeShakeDetected = System.currentTimeMillis()
            isShaking = true
            when {
                round(x, 4) > 30.0000 -> {
                    Log.d("sensor", "X Left axis: $x")
                    shakeListener.onShakeDetected("kiri")
                }
                round(x, 4) < -30.0000 -> {
                    Log.d("sensor", "X Right axis: $x")
                    shakeListener.onShakeDetected("kanan")
                }
            }
        } else {
            val timeDelta = System.currentTimeMillis() - lastTimeShakeDetected
            if (timeDelta > timeBeforeDeclaringShakeStopped && isShaking) {
                isShaking = false
                shakeListener.onShakeStopped()
            }
        }
    }

    private fun round(x: Float, Rpl: Int): Float {
        var xValues = x
        val p = Math.pow(10.0, Rpl.toDouble()).toFloat()
        xValues *= p
        val tmp = Math.round(xValues).toFloat()
        return tmp / p
    }

}