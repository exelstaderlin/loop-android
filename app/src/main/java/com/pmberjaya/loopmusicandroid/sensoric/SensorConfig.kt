package com.pmberjaya.loopmusicandroid.sensoric

import android.annotation.SuppressLint
import android.app.Activity

/**
 * Created by Exel staderlin on 7/1/2019.
 */
@SuppressLint("StaticFieldLeak")
object SensorConfig {
    var shake : Boolean ?= false
    var activity : Activity?= null
}