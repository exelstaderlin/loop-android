package com.pmberjaya.loopmusicandroid.sensoric

import android.hardware.Sensor.TYPE_PROXIMITY
import android.hardware.SensorEvent

/**
 * Created by Exel staderlin on 3/19/2019.
 */
class WaveDetector(var threshold : Float, var waveListener : WaveListener) : SensorDetector(TYPE_PROXIMITY){

    private var lastProximityEventTime: Long = 0
    private var lastProximityState: Int = 0

    interface WaveListener {
        fun onWave()
    }

    override fun onSensorEvent(sensorEvent: SensorEvent) {
        val distance = sensorEvent.values[0]
        val proximityState: Int
        val proximityFar = 0
        val proximityNear = 1

        proximityState = if (distance == 0f) {
            proximityNear
        } else {
            proximityFar
        }

        val now = System.currentTimeMillis()
        val eventDeltaMillis = now - this.lastProximityEventTime
        if (eventDeltaMillis < threshold
            && proximityNear == lastProximityState
            && proximityFar == proximityState
        ) {
            // Wave detected
            waveListener.onWave()
        }
        this.lastProximityEventTime = now
        this.lastProximityState = proximityState
    }


}