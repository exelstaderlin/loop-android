package com.pmberjaya.loopmusicandroid.sensoric

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import java.util.*

/**
 * Created by Exel staderlin on 6/28/2019.
 */
class SensorBuilder(var activity: Context) {

    private var sensorManager: SensorManager? = activity.applicationContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val defaultSensorsMap = HashMap<Any, SensorDetector>()
    private var shakeDetector : ShakeDetector ?= null
    private var waveDetector : WaveDetector ?= null

    fun unRegisterShakeListener() {
        sensorManager?.unregisterListener(shakeDetector)
    }

    fun unRegisterWaveListener() {
        sensorManager?.unregisterListener(waveDetector)
    }

    fun setShakeListener(listener: ShakeDetector.ShakeListener){
        shakeDetector = ShakeDetector(10F, 2000, listener)
        startLibrarySensorDetection(shakeDetector!!, activity)
    }

    fun setWaveListener(listener: WaveDetector.WaveListener) {
        waveDetector = WaveDetector(1000F, listener)
        startLibrarySensorDetection(waveDetector!!, activity)
    }

    fun setLightListener(listener: LightDetector.LightListener): SensorBuilder {
        val lightDetector = LightDetector(listener)
        startLibrarySensorDetection(lightDetector, activity)
        return SensorBuilder(activity)
    }

    private fun startLibrarySensorDetection(detector: SensorDetector, clientListener: Any) {
        if (!defaultSensorsMap.containsKey(clientListener)) {
            defaultSensorsMap[clientListener] = detector
            startSensorDetection(detector)
        }
    }

    private fun stopLibrarySensorDetection(detector: SensorDetector, clientListener: Any) {
        if (!defaultSensorsMap.containsKey(clientListener)) {
            defaultSensorsMap[clientListener] = detector
            stopSensorDetection(detector)
        }
    }

    private fun startSensorDetection(detector: SensorDetector) {
        val sensors = convertTypesToSensors(detector.getSensorTypes())
        registerDetectorForAllSensors(detector, sensors)
    }

    private fun stopSensorDetection(detector: SensorDetector) {
        val sensors = convertTypesToSensors(detector.getSensorTypes())
        unRegisterDetectorForAllSensors(detector, sensors)
    }


    private fun convertTypesToSensors(sensorTypes: IntArray): Iterable<Sensor> {
        val sensors = ArrayList<Sensor>()
        if (sensorManager != null) {
            for (sensorType in sensorTypes) {
                sensors.add(sensorManager!!.getDefaultSensor(sensorType))
            }
        }
        return sensors
    }

    private fun registerDetectorForAllSensors(detector: SensorDetector, sensors: Iterable<Sensor>) {
        for (sensor in sensors) {
            sensorManager?.registerListener(detector, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    private fun unRegisterDetectorForAllSensors(detector: SensorDetector, sensors: Iterable<Sensor>) {
        for (sensor in sensors) {
            sensorManager?.unregisterListener(detector)
        }
    }


}