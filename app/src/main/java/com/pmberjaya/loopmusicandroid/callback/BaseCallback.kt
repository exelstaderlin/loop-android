package com.pmberjaya.loopmusicandroid.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseCallback {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

}