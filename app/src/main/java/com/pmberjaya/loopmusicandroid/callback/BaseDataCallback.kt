package com.pmberjaya.loopmusicandroid.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class  BaseDataCallback<T>{

    @SerializedName("status")
    @Expose
    var status: Boolean = true

    @SerializedName("data")
    @Expose
    var data: T? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

}