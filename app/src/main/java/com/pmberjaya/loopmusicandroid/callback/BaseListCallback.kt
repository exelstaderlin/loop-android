package com.pmberjaya.loopmusicandroid.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class BaseListCallback<T> {
    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("data")
    @Expose
    var data: ArrayList<T> = ArrayList()

    @SerializedName("message")
    @Expose
    var message: String? = ""
}