package com.pmberjaya.loopmusicandroid.service

import com.pmberjaya.loopmusicandroid.callback.BaseCallback
import com.pmberjaya.loopmusicandroid.io.BaseRx
import com.pmberjaya.loopmusicandroid.io.RestClient
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 7/15/2019.
 */
object ServicesManager {

    private var running = false
    private var playing = false

    private var ms = MusicService()


    var isPlaying: Boolean
        get() {
            return playing
        }
        set(value) {
            playing = value
        }


    fun getRunningService(): Boolean {
        return running
    }

    fun setRunningService(boolean: Boolean) {
        running = boolean
    }

    fun postUserLog(token: String, userId : Int, songId : Int) {
        val observable = RestClient.getApiInterface(token).postUserLog(userId, songId)
        BaseRx<BaseCallback>().requestNoList(observable, object : DisposableObserver<BaseCallback>() {
            override fun onComplete() {}
            override fun onNext(t: BaseCallback) {}
            override fun onError(e: Throwable) {}
        })
    }


}