package com.pmberjaya.loopmusicandroid.service

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.media.app.NotificationCompat.*
import com.pmberjaya.loopmusicandroid.R
import com.pmberjaya.loopmusicandroid.app.music.MusicPlayerActivity
import com.pmberjaya.loopmusicandroid.app.setting.equalizer.EqualizerFragment
import com.pmberjaya.loopmusicandroid.miband.Miband
import com.pmberjaya.loopmusicandroid.miband.MibandCallback
import com.pmberjaya.loopmusicandroid.miband.listener.HeartrateListener
import com.pmberjaya.loopmusicandroid.util.SessionManager
import com.pmberjaya.loopmusicandroid.utilities.Utility


/**
 * Created by Exel staderlin on 7/11/2019.
 */

const val CHANNEL_ID = "ForegroundServiceChannel"
const val CHANNEL_BROADCAST = "musicService"

class MusicService : Service(), AudioManager.OnAudioFocusChangeListener {

    private val mediaPlayer = MediaPlayer()
    private var mSessionManager: SessionManager? = null
    private val musicBind = MusicBinder()
    private var focus: Int = 0
    private var millisRemaining: Long = 16000
    private var counterTimer: CountDownTimer? = null
    private var miband = Miband(this)
    private lateinit var mBluetoothAdapter:
            BluetoothAdapter

    val duration: Int
        get() = mediaPlayer.duration

    val audioSessionId: Int
        get() = mediaPlayer.audioSessionId

    val currentPosition: Int
        get() = mediaPlayer.currentPosition

    private val heartRateNotifyListener = HeartrateListener { heartRate ->
        if (mSessionManager?.switchSleepTracking!!) {
            if (heartRate < 90) {
                ServicesManager.isPlaying = false
                mediaPlayer.pause()
                counterTimer?.cancel()
                focus = 0
            }
        }

    }

    private val mibandCallback = object : MibandCallback {
        override fun onSuccess(data: Any?, status: Int) {
            when (status) {
                MibandCallback.STATUS_SEARCH_DEVICE -> {
                    Log.e("TAG : ", "성공: STATUS_SEARCH_DEVICE")
                    miband.connect(data as BluetoothDevice, this)
                }
                MibandCallback.STATUS_CONNECT -> {
                    Log.e("TAG : ", "성공: STATUS_CONNECT")
                    miband.setHeartRateScanListener(heartRateNotifyListener, this)
                }
                MibandCallback.STATUS_START_HEARTRATE_SCAN -> {
                    Log.e("TAG : ", "성공: STATUS_START_HEARTRATE_SCAN")
                }
            }
        }

        override fun onFail(errorCode: Int, msg: String, status: Int) {
            when (status) {
                MibandCallback.STATUS_SEARCH_DEVICE -> Log.e("TAG : ", "Fail: STATUS_SEARCH_DEVICE")
                MibandCallback.STATUS_CONNECT -> Log.e("TAG : ", "Fail: STATUS_CONNECT")
            }
        }
    }

    inner class MusicBinder : Binder() {
        val service: MusicService
            get() = this@MusicService
    }

    override fun onCreate() {
        super.onCreate()
        Log.d("service", "onCreate")
        try {
            mBluetoothAdapter =
                (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
        } catch (e: Exception) {
            e.stackTrace
        }
        mSessionManager = SessionManager(this)
        mediaPlayer.reset()
        mediaPlayer.setVolume(100f, 100f)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        createNotificationChannel()
        ServicesManager.setRunningService(true)
        try {
            val url = intent.getStringExtra("url") ?: ""
            val audio = intent.getStringExtra("audio")
            playMediaPlayer(audio, url)
            checkSleepTrackingFeature()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        startForeground(1, notificationCompat())
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        binderEqualizer(musicBind)
        return musicBind
    }

    override fun onAudioFocusChange(focusChange: Int) {
        if (focusChange <= 0) {
            if (focus == 1) {
                mediaPlayer.pause()
            }
        } else {
            if (focus == 1) {
                mediaPlayer.start()
            }
        }
    }

    fun seekTo(sec: Int) {
        mediaPlayer.seekTo(sec)
    }

    private fun countDownTimer(millisInFuture: Long): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                millisRemaining = millisUntilFinished
                Log.d("seconds remaining: ", (millisUntilFinished / 1000).toString())
            }

            override fun onFinish() {
                counterTimer = null
                ServicesManager.postUserLog(
                    "", mSessionManager?.account?.id ?: 0,
                    mSessionManager?.playingSong?.id ?: 0
                )// POST USER LOG
                Log.d("seconds remaining: ", "Done")
                miband.startHeartRateScan(0, mibandCallback)
            }
        }
    }

    private fun playMediaPlayer(audio: String, url: String) {
        when (audio) {
            "play" -> {
                counterTimer?.cancel()
                mediaPlayer.reset()
//                mediaPlayer.setDataSource(url)
                val uri = Uri.parse("android.resource://" + packageName + "/" + R.raw.usa_pop_2009)
                mediaPlayer.setDataSource(applicationContext, uri)
                mediaPlayer.prepareAsync()
                mediaPlayer.setOnPreparedListener {
                    it.start()
                    counterTimer = countDownTimer(16000).start()
                }
                mediaPlayer.setOnCompletionListener {
                    Intent().also { intent ->
                        intent.action = CHANNEL_BROADCAST
                        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
                    }
                }
                focus = 1
            }
            "resume" -> {
                mediaPlayer.start()
                if (counterTimer != null) {
                    counterTimer = countDownTimer(millisRemaining).start()
                }
                focus = 1
            }
            "pause" -> {
                mediaPlayer.pause()
                counterTimer?.cancel()
                focus = 0
            }
        }
    }

    private fun checkSleepTrackingFeature() {
        if (mSessionManager?.switchSleepTracking!!) {
            miband.searchDevice(mBluetoothAdapter, this.mibandCallback)
            miband.setDisconnectedListener {
                miband.searchDevice(
                    mBluetoothAdapter,
                    mibandCallback
                )
            }
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_LOW
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager?.createNotificationChannel(serviceChannel)
        }
    }

    private fun notificationCompat(): Notification? {
        val notificationIntent = Intent(this, MusicPlayerActivity::class.java).apply {
            putExtra("data", mSessionManager?.playingSong)
            putIntegerArrayListExtra("list_song_id", mSessionManager?.listSongId)
            putParcelableArrayListExtra("list_song_temp", mSessionManager?.listSongTemp)
            putExtra("from", "main")
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT
        )
        val mediaSession = MediaSessionCompat(applicationContext, "tag")

        val bmp = Utility.getBitmap(mSessionManager?.playingSong?.artist?.image)
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(mSessionManager?.playingSong?.title + " ● " + mSessionManager?.playingSong?.artist?.name)
            .setContentText("Is playing now")
            .setSmallIcon(R.drawable.ic_loop_logo)
            .setLargeIcon(bmp)
            .setContentIntent(pendingIntent)
            .setStyle(MediaStyle().setMediaSession(mediaSession.sessionToken))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .build()


    }

    private fun binderEqualizer(binder: MusicBinder) {
        val sessionId = binder.service.audioSessionId
        EqualizerFragment.newBuilder()
            .setAccentColor(Color.parseColor("#4caf50"))
            .setAudioSessionId(sessionId)
            .build()
    }

    override fun onDestroy() {
        mSessionManager?.clearPlayingSong()
        ServicesManager.setRunningService(false)
        ServicesManager.isPlaying = false
        counterTimer?.cancel()
        Log.d("service", "OnDestroy")
        mediaPlayer.stop()
        mediaPlayer.release()
        super.onDestroy()
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        mSessionManager?.clearPlayingSong()
        ServicesManager.setRunningService(false)
        ServicesManager.isPlaying = false
        counterTimer?.cancel()
        Log.e("ClearFromRecentService", "END")
        mediaPlayer.stop()
        mediaPlayer.release()
        super.onDestroy()
        stopSelf()
    }

}

