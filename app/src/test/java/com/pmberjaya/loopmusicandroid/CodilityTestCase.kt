package com.pmberjaya.loopmusicandroid

import org.junit.Test

class CodilityTestCase {

    @Test
    fun binaryGap() {
        var gap = 0
        var tempGap = 0
        var startCount = false
        val binaryStr = Integer.toBinaryString(1041)
        println(binaryStr)
        for (i in binaryStr.indices) {
            if (binaryStr[i].toString() == "1")
                startCount = true
            if (startCount) {
                if (binaryStr[i].toString() == "0")
                    gap++
                if (binaryStr[i].toString() == "1") {
                    if (tempGap < gap) {
                        tempGap = gap
                    }
                    gap = 0
                }
            }
        }
        print(tempGap)
    }

    @Test
    fun solution() {
        var arr: IntArray = intArrayOf()
        val rotate = 3
        if(arr.size != 0) {
            for (i in 0 until rotate) {
                var temp = intArrayOf()
                val lastValue = arr[arr.size - 1]
                temp = temp.plus(lastValue)
                for (j in 0 until arr.size - 1) {
                    temp = temp.plus(arr[j])

                }
                arr = temp
            }

        }
        print(arr.joinToString())
    }

    @Test
    fun stars() {
        var star = "*"
        for (i in 0 until 5) {
            println(star)
            star += "*"
        }
        for (i in 0 until 5) {
            val minStar = star.substring(i+2)
            println(minStar)
        }
    }

    @Test
    fun oddOccurrancesArray() {
        val arr: IntArray = intArrayOf(9, 3, 9, 3, 9, 7, 9)
        var unPaired = 0
        for(item in arr) {
            unPaired = unPaired xor item
        }
        print(unPaired)
    }


}