package com.pmberjaya.loopmusicandroid

import org.junit.Test
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL


class BotSearchImageKotlin {

    @Test
    fun stringLenghtPrint() {
        val str = "https://i.guim.co.uk/img/static/sys-images/Guardian/Pix/pictures/2010/10/20/1287582744736/Paul-Natkin-Archive-006.jpg?width=1200&height=630&quality=85&auto=format&fit=crop&overlay-align=bottom%2Cleft&overlay-width=100p&overlay-base64=L2ltZy9zdGF0aWMvb3ZlcmxheXMvdGctZGVmYXVsdC5wbmc&enable=upscale&s=5e43698b45d0526932e924ec759687c9"
        print(str.length)
    }

    @Test
    fun googleSearchBot() {
        val key = "AIzaSyCuONXnwnIKSu7-UlsATXeBovxwGmaU9fw"
        val qry = "Android"
        val url = URL(
            "https://www.googleapis.com/customsearch/v1?key=$key&cx=013036536707430787589:_pqjad5hr1a&q=$qry&as_filetype=png"
        )
        val conn: HttpURLConnection = url.openConnection() as HttpURLConnection
        conn.requestMethod = "GET"
        conn.setRequestProperty("Accept", "application/json")
        val br = BufferedReader(
            InputStreamReader(
                conn.inputStream
            )
        )

        var output: String
        println("Output from Server .... \n")
//        while (br.readLine().also { output = it } != null) {
//            println(output)
//        }
        while (br.readLine().also { output = it } != null) {
            if (output.contains("\"og:image\": \"")) {
                try {
                    val link = output.substring(25, output.indexOf("\",")) //25 sudah saya itung index keberapa yg mau saya hilangkan
                    println(link) //Will print the google search links
                }catch (e : Exception) {
                    println(output) //Will print the google search links
                }
            }
        }
        conn.disconnect()
    }
}
