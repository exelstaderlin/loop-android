package com.pmberjaya.loopmusicandroid

import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun testArrayMin() {
        val arr = arrayListOf(1, 1, 1, 2, 2, 3, 4, 5, 5, 8, 8)
        var angkaTemp = "0"
        var arrStr = ""
        for (i in arr.indices) {
            var hasil: String
            arrStr += if (arr[i].toString() == angkaTemp) {
                hasil = "-"
                "$hasil,"
            } else {
                hasil = arr[i].toString()
                "$hasil,"
            }
            angkaTemp = hasil
        }
        println(arrStr)
    }

    @Test
    fun testStr() {
        val a = "ayam*"
        val b = "ayam"
        var tempInput = ""
        var bintang = ""
        var kiri = ""
        var kanan = ""
        var ketemuBintang = false
        for (i in a.indices) {
            if (a[i].toString() == "*") {
                ketemuBintang = true
            }
            if (!ketemuBintang)
                kiri += a[i].toString()
            else
                if (a[i].toString() != "*")
                    kanan += a[i].toString()
        }
        var ki = false
        for (i in b.indices) {
            tempInput += b[i]
            if (tempInput == kiri) {
                ki = true
            }
            if (kanan == b[i].toString()) {
                if(ki == true) {
                    bintang = bintang.substring(0, bintang.length - 1)
                }
                break
            }
            if (ki == true)
                bintang += b[i + 1]

        }
        var hasil = ""
        for (i in a.indices) {
            if (a[i].toString() == "*") {
                hasil += bintang
            } else {
                hasil += a[i].toString()
            }
        }
        if(hasil == b)
            println("bisa")
        else
            println("tidak bisa")

        println("a = $a")
        println("b = $b")
        println("bintang = $bintang")
    }

    @Test
    fun testingFun() {
//        val eqIntArr = EqualizerSettings.seekbarpos
//        var eqValueStr = ""
//        for (i in eqIntArr.indices) {
//            eqValueStr += (eqIntArr[i].toString() + ",")
//        }
//        println(eqValueStr)
        setEqValueArr("600,0,0,0,0,")
    }

    @Test
    fun abbreviateStr() {
        val str = "! Www.Polskie-Mp3.Tk ! Jacek Kaczmarski"
        println(str.length)
        if(str.length > 30)
            print(str.substring(0,30) + "...")
    }


    private fun setEqValueArr(eqValueStr: String) {
        val exampleIntArr = ArrayList<Int>()
        var tempStr = ""
        for (i in eqValueStr.indices) {
            val value = eqValueStr[i].toString()
            if (value != ",")
                tempStr += value
            else{
                exampleIntArr.add(tempStr.toInt())
                tempStr = ""
            }
        }
        print(exampleIntArr)
    }

    private fun convertToIntArray(integers: List<Int>): IntArray {
        val ret = IntArray(integers.size)
        for (i in ret.indices)
            ret[i] = integers[i]
        return ret
    }
}


